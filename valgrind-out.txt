==32548== Memcheck, a memory error detector
==32548== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32548== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32548== Command: ./ElpisRuntime
==32548== Parent PID: 29845
==32548== 
--32548-- 
--32548-- Valgrind options:
--32548--    --leak-check=full
--32548--    --error-exitcode=1
--32548--    --show-leak-kinds=all
--32548--    --track-origins=yes
--32548--    --verbose
--32548--    --show-reachable=no
--32548--    --track-origins=yes
--32548--    --log-file=valgrind-out.txt
--32548-- Contents of /proc/version:
--32548--   Linux version 5.3.0-40-generic (buildd@lcy01-amd64-024) (gcc version 7.4.0 (Ubuntu 7.4.0-1ubuntu1~18.04.1)) #32~18.04.1-Ubuntu SMP Mon Feb 3 14:05:59 UTC 2020
--32548-- 
--32548-- Arch and hwcaps: AMD64, LittleEndian, amd64-cx16-lzcnt-rdtscp-sse3-avx-avx2-bmi
--32548-- Page sizes: currently 4096, max supported 4096
--32548-- Valgrind library directory: /usr/lib/valgrind
--32548-- Reading syms from /home/keyhan/projects/c++/EogenVirtualMachine/ElpisRuntime
--32548-- Reading syms from /lib/x86_64-linux-gnu/ld-2.27.so
--32548--   Considering /lib/x86_64-linux-gnu/ld-2.27.so ..
--32548--   .. CRC mismatch (computed 1b7c895e wanted 2943108a)
--32548--   Considering /usr/lib/debug/lib/x86_64-linux-gnu/ld-2.27.so ..
--32548--   .. CRC is valid
--32548-- Reading syms from /usr/lib/valgrind/memcheck-amd64-linux
--32548--   Considering /usr/lib/valgrind/memcheck-amd64-linux ..
--32548--   .. CRC mismatch (computed 41ddb025 wanted 9972f546)
--32548--    object doesn't have a symbol table
--32548--    object doesn't have a dynamic symbol table
--32548-- Scheduler: using generic scheduler lock implementation.
--32548-- Reading suppressions file: /usr/lib/valgrind/default.supp
==32548== embedded gdbserver: reading from /tmp/vgdb-pipe-from-vgdb-to-32548-by-keyhan-on-???
==32548== embedded gdbserver: writing to   /tmp/vgdb-pipe-to-vgdb-from-32548-by-keyhan-on-???
==32548== embedded gdbserver: shared mem   /tmp/vgdb-pipe-shared-mem-vgdb-32548-by-keyhan-on-???
==32548== 
==32548== TO CONTROL THIS PROCESS USING vgdb (which you probably
==32548== don't want to do, unless you know exactly what you're doing,
==32548== or are doing some strange experiment):
==32548==   /usr/lib/valgrind/../../bin/vgdb --pid=32548 ...command...
==32548== 
==32548== TO DEBUG THIS PROCESS USING GDB: start GDB like this
==32548==   /path/to/gdb ./ElpisRuntime
==32548== and then give GDB the following command
==32548==   target remote | /usr/lib/valgrind/../../bin/vgdb --pid=32548
==32548== --pid is optional if only one valgrind process is running
==32548== 
--32548-- REDIR: 0x401f2f0 (ld-linux-x86-64.so.2:strlen) redirected to 0x580608c1 (???)
--32548-- REDIR: 0x401f0d0 (ld-linux-x86-64.so.2:index) redirected to 0x580608db (???)
--32548-- Reading syms from /usr/lib/valgrind/vgpreload_core-amd64-linux.so
--32548--   Considering /usr/lib/valgrind/vgpreload_core-amd64-linux.so ..
--32548--   .. CRC mismatch (computed 50df1b30 wanted 4800a4cf)
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so
--32548--   Considering /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so ..
--32548--   .. CRC mismatch (computed f893b962 wanted 95ee359e)
--32548--    object doesn't have a symbol table
==32548== WARNING: new redirection conflicts with existing -- ignoring it
--32548--     old: 0x0401f2f0 (strlen              ) R-> (0000.0) 0x580608c1 ???
--32548--     new: 0x0401f2f0 (strlen              ) R-> (2007.0) 0x04c32db0 strlen
--32548-- REDIR: 0x401d360 (ld-linux-x86-64.so.2:strcmp) redirected to 0x4c33ee0 (strcmp)
--32548-- REDIR: 0x401f830 (ld-linux-x86-64.so.2:mempcpy) redirected to 0x4c374f0 (mempcpy)
--32548-- Reading syms from /lib/x86_64-linux-gnu/libm-2.27.so
--32548--   Considering /lib/x86_64-linux-gnu/libm-2.27.so ..
--32548--   .. CRC mismatch (computed 7feae033 wanted b29b2508)
--32548--   Considering /usr/lib/debug/lib/x86_64-linux-gnu/libm-2.27.so ..
--32548--   .. CRC is valid
--32548-- Reading syms from /lib/x86_64-linux-gnu/libuuid.so.1.3.0
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libpq.so.5.12
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /lib/x86_64-linux-gnu/libpthread-2.27.so
--32548--   Considering /usr/lib/debug/.build-id/28/c6aade70b2d40d1f0f3d0a1a0cad1ab816448f.debug ..
--32548--   .. build-id is valid
--32548-- Reading syms from /usr/local/lib/libcivetweb.so.1
--32548-- Reading syms from /lib/x86_64-linux-gnu/libc-2.27.so
--32548--   Considering /lib/x86_64-linux-gnu/libc-2.27.so ..
--32548--   .. CRC mismatch (computed b1c74187 wanted 042cc048)
--32548--   Considering /usr/lib/debug/lib/x86_64-linux-gnu/libc-2.27.so ..
--32548--   .. CRC is valid
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libssl.so.1.1
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libcrypto.so.1.1
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libgssapi_krb5.so.2.2
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libldap_r-2.4.so.2.10.8
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /lib/x86_64-linux-gnu/libdl-2.27.so
--32548--   Considering /lib/x86_64-linux-gnu/libdl-2.27.so ..
--32548--   .. CRC mismatch (computed bd82fa02 wanted d1fdccc9)
--32548--   Considering /usr/lib/debug/lib/x86_64-linux-gnu/libdl-2.27.so ..
--32548--   .. CRC is valid
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libkrb5.so.3.3
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libk5crypto.so.3.1
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /lib/x86_64-linux-gnu/libcom_err.so.2.1
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libkrb5support.so.0.1
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/liblber-2.4.so.2.10.8
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /lib/x86_64-linux-gnu/libresolv-2.27.so
--32548--   Considering /lib/x86_64-linux-gnu/libresolv-2.27.so ..
--32548--   .. CRC mismatch (computed 4f654d8d wanted bb34a537)
--32548--   Considering /usr/lib/debug/lib/x86_64-linux-gnu/libresolv-2.27.so ..
--32548--   .. CRC is valid
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libsasl2.so.2.0.25
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libgssapi.so.3.0.0
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libgnutls.so.30.14.10
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /lib/x86_64-linux-gnu/libkeyutils.so.1.5
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libheimntlm.so.0.1.0
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libkrb5.so.26.0.0
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libasn1.so.8.0.0
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libhcrypto.so.4.1.0
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libroken.so.18.1.0
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /lib/x86_64-linux-gnu/libz.so.1.2.11
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libp11-kit.so.0.3.0
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libidn2.so.0.3.3
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libunistring.so.2.1.0
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libtasn1.so.6.5.5
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libnettle.so.6.4
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libhogweed.so.4.4
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libgmp.so.10.3.2
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libwind.so.0.0.0
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libheimbase.so.1.0.0
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libhx509.so.5.0.0
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libsqlite3.so.0.8.6
--32548--    object doesn't have a symbol table
--32548-- Reading syms from /lib/x86_64-linux-gnu/libcrypt-2.27.so
--32548--   Considering /lib/x86_64-linux-gnu/libcrypt-2.27.so ..
--32548--   .. CRC mismatch (computed 8955bf71 wanted 17b36cbd)
--32548--   Considering /usr/lib/debug/lib/x86_64-linux-gnu/libcrypt-2.27.so ..
--32548--   .. CRC is valid
--32548-- Reading syms from /usr/lib/x86_64-linux-gnu/libffi.so.6.0.4
--32548--    object doesn't have a symbol table
--32548-- REDIR: 0x5b10c70 (libc.so.6:memmove) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b0fd40 (libc.so.6:strncpy) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b10f50 (libc.so.6:strcasecmp) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b0f790 (libc.so.6:strcat) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b0fd70 (libc.so.6:rindex) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b127c0 (libc.so.6:rawmemchr) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b10de0 (libc.so.6:mempcpy) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b10c10 (libc.so.6:bcmp) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b0fd00 (libc.so.6:strncmp) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b0f800 (libc.so.6:strcmp) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b10d40 (libc.so.6:memset) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b2e0f0 (libc.so.6:wcschr) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b0fca0 (libc.so.6:strnlen) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b0f870 (libc.so.6:strcspn) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b10fa0 (libc.so.6:strncasecmp) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b0f840 (libc.so.6:strcpy) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b110e0 (libc.so.6:memcpy@@GLIBC_2.14) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b0fda0 (libc.so.6:strpbrk) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b0f7c0 (libc.so.6:index) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b0fc70 (libc.so.6:strlen) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b1a6c0 (libc.so.6:memrchr) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b10ff0 (libc.so.6:strcasecmp_l) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b10be0 (libc.so.6:memchr) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b2eeb0 (libc.so.6:wcslen) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b10050 (libc.so.6:strspn) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b10f20 (libc.so.6:stpncpy) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b10ef0 (libc.so.6:stpcpy) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b127f0 (libc.so.6:strchrnul) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b11040 (libc.so.6:strncasecmp_l) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5ba38a0 (libc.so.6:__memcpy_chk) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b10b20 (libc.so.6:strstr) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5b0fcd0 (libc.so.6:strncat) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
--32548-- REDIR: 0x5ba3970 (libc.so.6:__memmove_chk) redirected to 0x4a2a6e0 (_vgnU_ifunc_wrapper)
==32548== WARNING: new redirection conflicts with existing -- ignoring it
--32548--     old: 0x05c00ac0 (__memcpy_chk_avx_una) R-> (2030.0) 0x04c375e0 __memcpy_chk
--32548--     new: 0x05c00ac0 (__memcpy_chk_avx_una) R-> (2024.0) 0x04c36fa0 __memmove_chk
==32548== WARNING: new redirection conflicts with existing -- ignoring it
--32548--     old: 0x05c00ac0 (__memcpy_chk_avx_una) R-> (2030.0) 0x04c375e0 __memcpy_chk
--32548--     new: 0x05c00ac0 (__memcpy_chk_avx_una) R-> (2024.0) 0x04c36fa0 __memmove_chk
==32548== WARNING: new redirection conflicts with existing -- ignoring it
--32548--     old: 0x05c00ac0 (__memcpy_chk_avx_una) R-> (2030.0) 0x04c375e0 __memcpy_chk
--32548--     new: 0x05c00ac0 (__memcpy_chk_avx_una) R-> (2024.0) 0x04c36fa0 __memmove_chk
--32548-- REDIR: 0x5c003c0 (libc.so.6:__strrchr_avx2) redirected to 0x4c32730 (rindex)
--32548-- REDIR: 0x5c00590 (libc.so.6:__strlen_avx2) redirected to 0x4c32cf0 (strlen)
--32548-- REDIR: 0x5bf7510 (libc.so.6:__strncmp_sse42) redirected to 0x4c33570 (__strncmp_sse42)
--32548-- REDIR: 0x5b09070 (libc.so.6:malloc) redirected to 0x4c2faa0 (malloc)
--32548-- REDIR: 0x5c00ad0 (libc.so.6:__memcpy_avx_unaligned_erms) redirected to 0x4c366e0 (memmove)
--32548-- REDIR: 0x5bdbd60 (libc.so.6:__strcmp_ssse3) redirected to 0x4c33da0 (strcmp)
--32548-- REDIR: 0x5b0c030 (libc.so.6:calloc) redirected to 0x4c31a70 (calloc)
--32548-- REDIR: 0x5bfffa0 (libc.so.6:__strchr_avx2) redirected to 0x4c32950 (index)
--32548-- REDIR: 0x5c00ac0 (libc.so.6:__memcpy_chk_avx_unaligned_erms) redirected to 0x4c375e0 (__memcpy_chk)
--32548-- REDIR: 0x5b09950 (libc.so.6:free) redirected to 0x4c30cd0 (free)
--32548-- REDIR: 0x5c001d0 (libc.so.6:__strchrnul_avx2) redirected to 0x4c37020 (strchrnul)
--32548-- REDIR: 0x5b10590 (libc.so.6:__GI_strstr) redirected to 0x4c37760 (__strstr_sse2)
--32548-- REDIR: 0x5c00f50 (libc.so.6:__memset_avx2_unaligned_erms) redirected to 0x4c365d0 (memset)
--32548-- REDIR: 0x5bed950 (libc.so.6:__strcpy_ssse3) redirected to 0x4c32dd0 (strcpy)
--32548-- REDIR: 0x5b239e0 (libc.so.6:__strcat_ssse3) redirected to 0x4c32990 (strcat)
--32548-- REDIR: 0x5b0ac30 (libc.so.6:realloc) redirected to 0x4c31cb0 (realloc)
--32548-- REDIR: 0x5bfc6f0 (libc.so.6:__rawmemchr_avx2) redirected to 0x4c37050 (rawmemchr)
--32548-- REDIR: 0x5bfc420 (libc.so.6:__memchr_avx2) redirected to 0x4c33f80 (memchr)
--32548-- REDIR: 0x5c00ab0 (libc.so.6:__mempcpy_avx_unaligned_erms) redirected to 0x4c37130 (mempcpy)
--32548-- REDIR: 0x5c00720 (libc.so.6:__strnlen_avx2) redirected to 0x4c32c90 (strnlen)
--32548-- REDIR: 0x5bfcba0 (libc.so.6:__memcmp_avx2_movbe) redirected to 0x4c35e00 (bcmp)
==32548== 
==32548== Process terminating with default action of signal 2 (SIGINT)
==32548==    at 0x5B820B4: read (read.c:27)
==32548==    by 0x5AFF147: _IO_file_underflow@@GLIBC_2.2.5 (fileops.c:531)
==32548==    by 0x5B003F1: _IO_default_uflow (genops.c:380)
==32548==    by 0x5AFA00C: getchar (getchar.c:39)
==32548==    by 0x12D90D: http_server_start (HttpServer.c:602)
==32548==    by 0x10EEDA: routeAndResolve (Kasper.c:870)
==32548==    by 0x1290A5: ride (Kasper.c:5489)
==32548==    by 0x129A48: executeIntern (Kasper.c:5610)
==32548==    by 0x129CCD: loadCodeFile (Kasper.c:5665)
==32548==    by 0x12A2A1: execute (Kasper.c:5758)
==32548==    by 0x10B0F0: main (main.c:6)
==32548== 
==32548== HEAP SUMMARY:
==32548==     in use at exit: 13,415,368 bytes in 91,264 blocks
==32548==   total heap usage: 384,850 allocs, 293,586 frees, 84,236,138 bytes allocated
==32548== 
==32548== Searching for pointers to 91,264 not-freed blocks
==32548== Checked 17,451,512 bytes
==32548== 
==32548== 304 bytes in 1 blocks are possibly lost in loss record 717 of 1,481
==32548==    at 0x4C31B25: calloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==32548==    by 0x40134A6: allocate_dtv (dl-tls.c:286)
==32548==    by 0x40134A6: _dl_allocate_tls (dl-tls.c:530)
==32548==    by 0x5637227: allocate_stack (allocatestack.c:627)
==32548==    by 0x5637227: pthread_create@@GLIBC_2.2.5 (pthread_create.c:644)
==32548==    by 0x5854F2F: mg_start_thread_with_id (in /usr/local/lib/libcivetweb.so.1)
==32548==    by 0x58679B0: mg_start2.constprop.45 (in /usr/local/lib/libcivetweb.so.1)
==32548==    by 0x5868359: mg_start (in /usr/local/lib/libcivetweb.so.1)
==32548==    by 0x12D84D: http_server_start (HttpServer.c:587)
==32548==    by 0x10EEDA: routeAndResolve (Kasper.c:870)
==32548==    by 0x1290A5: ride (Kasper.c:5489)
==32548==    by 0x129A48: executeIntern (Kasper.c:5610)
==32548==    by 0x129CCD: loadCodeFile (Kasper.c:5665)
==32548==    by 0x12A2A1: execute (Kasper.c:5758)
==32548== 
==32548== 1,088 bytes in 1 blocks are definitely lost in loss record 977 of 1,481
==32548==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==32548==    by 0x129BDB: readCodeFile (Kasper.c:5646)
==32548==    by 0x129C98: loadCodeFile (Kasper.c:5662)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x12A2A1: execute (Kasper.c:5758)
==32548==    by 0x10B0F0: main (main.c:6)
==32548== 
==32548== 1,398 bytes in 2 blocks are definitely lost in loss record 1,013 of 1,481
==32548==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==32548==    by 0x129BDB: readCodeFile (Kasper.c:5646)
==32548==    by 0x129C98: loadCodeFile (Kasper.c:5662)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x12A2A1: execute (Kasper.c:5758)
==32548==    by 0x10B0F0: main (main.c:6)
==32548== 
==32548== 3,672 bytes in 1 blocks are definitely lost in loss record 1,222 of 1,481
==32548==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==32548==    by 0x129FCA: execute (Kasper.c:5723)
==32548==    by 0x10B0F0: main (main.c:6)
==32548== 
==32548== 8,231 bytes in 2 blocks are definitely lost in loss record 1,362 of 1,481
==32548==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==32548==    by 0x129BDB: readCodeFile (Kasper.c:5646)
==32548==    by 0x129C98: loadCodeFile (Kasper.c:5662)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x12A2A1: execute (Kasper.c:5758)
==32548==    by 0x10B0F0: main (main.c:6)
==32548== 
==32548== 15,200 bytes in 50 blocks are possibly lost in loss record 1,412 of 1,481
==32548==    at 0x4C31B25: calloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==32548==    by 0x40134A6: allocate_dtv (dl-tls.c:286)
==32548==    by 0x40134A6: _dl_allocate_tls (dl-tls.c:530)
==32548==    by 0x5637227: allocate_stack (allocatestack.c:627)
==32548==    by 0x5637227: pthread_create@@GLIBC_2.2.5 (pthread_create.c:644)
==32548==    by 0x5854F2F: mg_start_thread_with_id (in /usr/local/lib/libcivetweb.so.1)
==32548==    by 0x58679F7: mg_start2.constprop.45 (in /usr/local/lib/libcivetweb.so.1)
==32548==    by 0x5868359: mg_start (in /usr/local/lib/libcivetweb.so.1)
==32548==    by 0x12D84D: http_server_start (HttpServer.c:587)
==32548==    by 0x10EEDA: routeAndResolve (Kasper.c:870)
==32548==    by 0x1290A5: ride (Kasper.c:5489)
==32548==    by 0x129A48: executeIntern (Kasper.c:5610)
==32548==    by 0x129CCD: loadCodeFile (Kasper.c:5665)
==32548==    by 0x12A2A1: execute (Kasper.c:5758)
==32548== 
==32548== 24,164 bytes in 4 blocks are definitely lost in loss record 1,437 of 1,481
==32548==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==32548==    by 0x129BDB: readCodeFile (Kasper.c:5646)
==32548==    by 0x129C98: loadCodeFile (Kasper.c:5662)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x12A2A1: execute (Kasper.c:5758)
==32548==    by 0x10B0F0: main (main.c:6)
==32548== 
==32548== 29,289 bytes in 12 blocks are definitely lost in loss record 1,442 of 1,481
==32548==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==32548==    by 0x129BDB: readCodeFile (Kasper.c:5646)
==32548==    by 0x129C98: loadCodeFile (Kasper.c:5662)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x12A2A1: execute (Kasper.c:5758)
==32548==    by 0x10B0F0: main (main.c:6)
==32548== 
==32548== 85,745 bytes in 13 blocks are definitely lost in loss record 1,456 of 1,481
==32548==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==32548==    by 0x129BDB: readCodeFile (Kasper.c:5646)
==32548==    by 0x129C98: loadCodeFile (Kasper.c:5662)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x129C79: loadCodeFile (Kasper.c:5659)
==32548==    by 0x12A2A1: execute (Kasper.c:5758)
==32548==    by 0x10B0F0: main (main.c:6)
==32548== 
==32548== LEAK SUMMARY:
==32548==    definitely lost: 153,587 bytes in 35 blocks
==32548==    indirectly lost: 0 bytes in 0 blocks
==32548==      possibly lost: 15,504 bytes in 51 blocks
==32548==    still reachable: 13,246,277 bytes in 91,178 blocks
==32548==         suppressed: 0 bytes in 0 blocks
==32548== Reachable blocks (those to which a pointer was found) are not shown.
==32548== To see them, rerun with: --leak-check=full --show-leak-kinds=all
==32548== 
==32548== ERROR SUMMARY: 9 errors from 9 contexts (suppressed: 0 from 0)
==32548== ERROR SUMMARY: 9 errors from 9 contexts (suppressed: 0 from 0)
