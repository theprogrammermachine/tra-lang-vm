
#include <cstdio>
#include <cstring>
#include <cstdlib>

#include <cstdint>

typedef struct {
    void* (*print)(char* text);
} ConsolePrinter;

void* print(char* text);
ConsolePrinter createConsolePrinter();