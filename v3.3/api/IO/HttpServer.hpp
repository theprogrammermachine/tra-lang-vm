
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include <netdb.h> // for getnameinfo()

// Usual socket headers
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <arpa/inet.h>
#include "../../models/Models.hpp"

#define SIZE 1024
#define BACKLOG 10  // Passed to listen()

void setHttpHeader(char httpHeader[]);
void start(void* (*serve)(), void* serveVCode, char* (*stringifyObj)(Object*));
void report(struct sockaddr_in *serverAddress);