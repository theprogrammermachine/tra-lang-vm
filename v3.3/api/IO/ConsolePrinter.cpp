
#include "ConsolePrinter.hpp"

void* print(char* text) {
    printf("%s\n", text);
}

ConsolePrinter createConsolePrinter() {
    ConsolePrinter printer;
    printer.print = print;
    return printer;
}