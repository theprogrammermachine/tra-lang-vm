
typedef struct {
    char* type{};
    int refCount = 0;
} Code;

struct Stack {
    Code base;
    int stackSize = 0;
    struct StackDataItem* item = NULL;
    void (*push)(struct Stack*, void*);
    void* (*top)(struct Stack*);
    void* (*pop)(struct Stack*);
    int (*size)(struct Stack*);
    bool (*isEmpty)(struct Stack*);
    struct StackDataItem* (*iterator)(struct Stack*);
};

struct List {
    Code base;
    int size{};
    struct ListDataItem* listPointer{};
    struct ListDataItem* iteratorPointer{};
    void  (*append)(struct List*, void*){};
    void* (*iteratorForward)(struct List*){};
    void* (*iteratorBackward)(struct List*){};
    bool  (*iteratorHasNext)(struct List*){};
    bool  (*iteratorHasBefore)(struct List*){};
};

typedef struct
{
    Code base;
    char* type{};
} Exp;

typedef struct {
    Exp exp;
    int valueType{};
} Value;

typedef struct {
    Value base{};
    char* value{};
} StringValue;

struct elt {
    struct elt *next;
    StringValue* key;
    void *value;
};

typedef struct {
    Code base;
    int size = 0;           /* size of the pointer table */
    int n = 0;              /* number of elements stored */
    struct elt **table{};
} Dictionary;

struct refelt {
    struct refelt *next;
    void *key;
    void *value;
};

typedef struct {
    Code base;
    int size{};           /* size of the pointer table */
    int n{};              /* number of elements stored */
    struct refelt **table{};
} RefDict;

typedef struct {
    Code base;
} Empty;

typedef struct
{
    Exp exp;
    Exp value1;
    char* value1Type{};
    Exp value2;
    char* value2Type{};
} Sum;

typedef struct
{
    Exp exp;
    Exp value1;
    char* value1Type{};
    Exp value2;
    char* value2Type{};
} Subtract;

typedef struct
{
    Exp exp;
    Exp value1;
    char* value1Type{};
    Exp value2;
    char* value2Type{};
} Multiply;

typedef struct
{
    Exp exp;
    Exp value1;
    char* value1Type{};
    Exp value2;
    char* value2Type{};
} Division;

typedef struct
{
    Exp exp;
    Exp value1;
    char* value1Type{};
    Exp value2;
    char* value2Type{};
} Mod;

typedef struct
{
    Exp exp;
    Exp value1;
    char* value1Type{};
    Exp value2;
    char* value2Type{};
} Pow;

typedef struct
{
    Exp exp;
    Exp value1;
    char* value1Type{};
    Exp value2;
    char* value2Type{};
} Equal;

typedef struct
{
    Exp exp;
    Exp value1;
    char* value1Type{};
    Exp value2;
    char* value2Type{};
} NE;

typedef struct
{
    Exp exp;
    Exp value1;
    char* value1Type{};
    Exp value2;
    char* value2Type{};
} LT;

typedef struct
{
    Exp exp;
    Exp value1;
    char* value1Type{};
    Exp value2;
    char* value2Type{};
} LE;

typedef struct
{
    Exp exp;
    Exp value1;
    char* value1Type{};
    Exp value2;
    char* value2Type{};
} GE;

typedef struct
{
    Exp exp;
    Exp value1;
    char* value1Type{};
    Exp value2;
    char* value2Type{};
} GT;

typedef struct
{
    Exp exp;
    Exp value1;
    char* value1Type{};
    Exp value2;
    char* value2Type{};
} And;

typedef struct
{
    Exp exp;
    Exp value1;
    char* value1Type{};
    Exp value2;
    char* value2Type{};
} Or;

typedef struct {
    Value base;
    double value{};
} DoubleValue;

typedef struct {
    Value base;
    float value{};
} FloatValue;

typedef struct {
    Value base;
    long value{};
} LongValue;

typedef struct {
    Value base;
    int value{};
} IntValue;

typedef struct {
    Value base;
    short value{};
} ShortValue;

typedef struct {
    Value base;
    bool value{};
} BoolValue;

typedef struct {
    Code base;
    Dictionary* value{};
    Dictionary* funcs{};
} Object;

typedef struct {
    Exp exp;
    char* id{};
} Identifier;

struct Reference {
    Code base;
    Identifier* currentChain{};
    void* restOfTheChain{};
};

struct Index {
    Code base;
    struct List* index{};
    void* var{};
    void* restOfTheChain{};
};

struct Period {
    Code base;
    void* start{};
    void* end{};
};

struct If {
    Code base;
    Exp* condition{};
    struct List* codes{};
    struct List* elseParts{};
};

typedef struct {
    Code base;
    Exp* condition{};
    struct List* codes{};
} ElseIf;

typedef struct {
    Code base;
    struct List* codes{};
} Else;

typedef struct {
    Code base;
    Exp condition;
    struct List* codes{};
} ConditionalLoop;

typedef struct {
    Code base;
    Identifier* counter{};
    Exp* limit{};
    Exp* step{};
    struct List* codes{};
} CounterLoop;

typedef struct {
    Code base;
    Code* funcRef{};
    Dictionary* entries{};
} Call;

typedef struct {
    Code base;
    Identifier* id{};
    unsigned long loc{};
    char* value{};
} Prop;

typedef struct {
    Code base;
    char* funcName{};
    struct List* params{};
    unsigned long loc{};
    char* codes{};
} Function;

typedef struct {
    struct List* params;
    char* body;
    unsigned long loc;
} Constructor;

typedef struct {
    Code base;
    char* className{};
    struct List* inheritance{};
    struct List* behavior{};
    struct List* properties{};
    Dictionary* functions{};
    Constructor* constructor{};
} Class;

typedef struct {
    Code base;
    char* code{};
    unsigned long loc{};
    unsigned long pointer{};
} CodePack;
