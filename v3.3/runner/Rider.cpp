
#include "Rider.hpp"

struct Stack codeLengthStack;
struct Stack bufferStack;
struct Stack expStack;
struct Stack dataStack;

static unsigned long codeLength = 0;
static char* code;
static unsigned long pointer = 0;

void* custom_alloc(size_t size) {
    void* p = malloc(size);
    notifyNewUsage(p);
    return p;
}

char* generate_uuid() {
    uuid_t binuuid;
    uuid_generate_random(binuuid);
    char *uuid = static_cast<char *>(custom_alloc(37));
    #ifdef capitaluuid
    uuid_unparse_upper(binuuid, uuid);
    #elif lowercaseuuid
    uuid_unparse_lower(binuuid, uuid);
    #else
    uuid_unparse(binuuid, uuid);
    #endif
    return uuid;
}

long measureLog10l(long input) {
    if (input == 0) return 0;
    else if (input < 0) input *= -1;
    return static_cast<long>(log10l(input));
}

float measureLog10f(float input) {
    if (input == 0) return 0;
    else if (input < 0) input *= -1;
    return log10f(input);
}

double measureLog10(double input) {
    if (input == 0) return 0;
    else if (input < 0) input *= -1;
    return log10(input);
}

void initStack(struct Stack* stack) {
    stack->item = nullptr;
    stack->base.type = const_cast<char*>("Stack");
    stack->stackSize = 0;
    stack->push = push;
    stack->pop = pop;
    stack->top = top;
    stack->size = size;
    stack->isEmpty = isEmpty;
    stack->iterator = iterator;
}

char* concat(const char *s1, char *s2)
{
    char *result = static_cast<char *>(custom_alloc(strlen(s1) + strlen(s2) + 1));
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

char* stringifyObject(Object* obj) {
    auto* stringify = (Function*) dict_get(obj->funcs, const_cast<char *>("stringify"));
    Dictionary* entriesDict = dict_new();
    dict_add(entriesDict, const_cast<char *>("this"), obj);
    if (stringify == nullptr) {
        struct ListDataItem* iterator = toList(obj->value)->listPointer;
        char* result = const_cast<char *>("{");
        while (iterator != nullptr) {
            Pair* pair = static_cast<Pair *>(iterator->data);
            result = concat(result, static_cast<char *>(pair->first));
            result = concat(result, const_cast<char *>(": "));
            if (strcmp(((Code*)pair->second)->type, "Object") == 0) {
                result = concat(result, stringifyObject((Object*)pair->second));
            } else if (strcmp(((Code*)pair->second)->type, "Array") == 0) {
                result = concat(result, stringifyArray(((Array*)pair->second)));
            }  else if ((*(Value*)pair->second).valueType == 0x01) {
                result = concat(result, (*(StringValue*)pair->second).value);
            } else if ((*(Value*)pair->second).valueType == 0x02) {
                ShortValue val1 = *(ShortValue*)pair->second;
                bool isNegative = false;
                if (val1.value < 0) {
                    val1.value *= -1;
                    isNegative = true;
                }
                char str[(int)(ceilf(measureLog10f(val1.value)+1)*sizeof(char))];
                sprintf(str, "%hd", val1.value);
                char* str2 = &str[0];
                if (isNegative) str2 = concat("-", str2);
                result = concat(result, str2);
            }
            else if ((*(Value*)pair->second).valueType == 0x06) {
                IntValue val1 = *(IntValue*)pair->second;
                bool isNegative = false;
                if (val1.value < 0) {
                    val1.value *= -1;
                    isNegative = true;
                }
                char str[(int)(ceil(measureLog10(val1.value)+1)*sizeof(char))];
                sprintf(str, "%d", val1.value);
                char* str2 = &str[0];
                if (isNegative) str2 = concat("-", str2);
                result = concat(result, str2);
            }
            else if ((*(Value*)pair->second).valueType == 0x03) {
                LongValue val1 = *(LongValue*)pair->second;
                bool isNegative = false;
                if (val1.value < 0) {
                    val1.value *= -1;
                    isNegative = true;
                }
                char str[(int)(ceill(measureLog10l(val1.value)+1)*sizeof(char))];
                sprintf(str, "%ld", val1.value);
                char* str2 = &str[0];
                if (isNegative) str2 = concat("-", str2);
                result = concat(result, str2);
            }
            else if ((*(Value*)pair->second).valueType == 0x04) {
                FloatValue val1 = *(FloatValue*)pair->second;
                char str[(int)(ceilf(measureLog10f(val1.value)+1)*sizeof(char))];
                sprintf(str, "%f", val1.value);
                char* str2 = &str[0];
                str2 = concat("-", str2);
                result = concat(result, str2);
            }
            else if ((*(Value*)pair->second).valueType == 0x05) {
                DoubleValue val1 = *(DoubleValue*)pair->second;
                bool isNegative = false;
                if (val1.value < 0) {
                    val1.value *= -1;
                    isNegative = true;
                }
                char str[(int)(ceil(measureLog10(val1.value)+1)*sizeof(char))];
                sprintf(str, "%f", val1.value);
                char* str2 = &str[0];
                if (isNegative) str2 = concat("-", str2);
                result = concat(result, str2);
            }
            if (iterator->prev != nullptr)
                result = concat(result, const_cast<char *>(", "));
            iterator = iterator->prev;
        }
        result = concat(result, const_cast<char *>("}"));
        return result;
    } else {
        auto *stringified = static_cast<StringValue *>(executeIntern(stringify->codes, stringify->loc,
                                                                            entriesDict));
        return stringified->value;
    }
}

char* stringifyArray(Array* array) {
    char* textToPrint = const_cast<char *>("[");
    for (unsigned long i = 0; i < array->used; i++) {
        void* arrayItem = array->array[i];
        if (strcmp(((Code*)arrayItem)->type, "Array") == 0) {
            char* stringified = stringifyArray((Array*)arrayItem);
            textToPrint = concat(textToPrint, stringified);
        }
        else if (strcmp(((Code*)arrayItem)->type, "Object") == 0) {
            auto* obj = (Object*) arrayItem;
            auto* stringify = (Function*) dict_get(obj->funcs, const_cast<char *>("stringify"));
            Dictionary* entriesDict = dict_new();
            dict_add(entriesDict, const_cast<char *>("this"), obj);
            if (stringify == nullptr) {
                struct ListDataItem* iterator = toList(obj->value)->listPointer;
                char* result = const_cast<char *>("{");
                while (iterator != nullptr) {
                    Pair* pair = static_cast<Pair *>(iterator->data);
                    result = concat(result, static_cast<char *>(pair->first));
                    result = concat(result, const_cast<char *>(": "));
                    if (strcmp(((Code*)pair->second)->type, "Object") == 0) {
                        result = concat(result, stringifyObject((Object*)pair->second));
                    } else if (strcmp(((Code*)pair->second)->type, "Array") == 0) {
                        result = concat(result, stringifyArray(((Array*)pair->second)));
                    }  else if ((*(Value*)pair->second).valueType == 0x01) {
                        result = concat(result, (*(StringValue*)pair->second).value);
                    } else if ((*(Value*)pair->second).valueType == 0x02) {
                        ShortValue val1 = *(ShortValue*)pair->second;
                        bool isNegative = false;
                        if (val1.value < 0) {
                            val1.value *= -1;
                            isNegative = true;
                        }
                        char str[(int)(ceilf(measureLog10f(val1.value)+1)*sizeof(char))];
                        sprintf(str, "%hd", val1.value);
                        char* str2 = &str[0];
                        if (isNegative) str2 = concat("-", str2);
                        result = concat(result, str2);
                    }
                    else if ((*(Value*)pair->second).valueType == 0x06) {
                        IntValue val1 = *(IntValue*)pair->second;
                        bool isNegative = false;
                        if (val1.value < 0) {
                            val1.value *= -1;
                            isNegative = true;
                        }
                        char str[(int)(ceil(measureLog10(val1.value)+1)*sizeof(char))];
                        sprintf(str, "%d", val1.value);
                        char* str2 = &str[0];
                        if (isNegative) str2 = concat("-", str2);
                        result = concat(result, str2);
                    }
                    else if ((*(Value*)pair->second).valueType == 0x03) {
                        LongValue val1 = *(LongValue*)pair->second;
                        bool isNegative = false;
                        if (val1.value < 0) {
                            val1.value *= -1;
                            isNegative = true;
                        }
                        char str[(int)(ceill(measureLog10l(val1.value)+1)*sizeof(char))];
                        sprintf(str, "%ld", val1.value);
                        char* str2 = &str[0];
                        if (isNegative) str2 = concat("-", str2);
                        result = concat(result, str2);
                    }
                    else if ((*(Value*)pair->second).valueType == 0x04) {
                        FloatValue val1 = *(FloatValue*)pair->second;
                        bool isNegative = false;
                        if (val1.value < 0) {
                            val1.value *= -1;
                            isNegative = true;
                        }
                        char str[(int)(ceilf(measureLog10f(val1.value)+1)*sizeof(char))];
                        sprintf(str, "%f", val1.value);
                        char* str2 = &str[0];
                        if (isNegative) str2 = concat("-", str2);
                        result = concat(result, str2);
                    }
                    else if ((*(Value*)pair->second).valueType == 0x05) {
                        DoubleValue val1 = *(DoubleValue*)pair->second;
                        bool isNegative = false;
                        if (val1.value < 0) {
                            val1.value *= -1;
                            isNegative = true;
                        }
                        char str[(int)(ceil(measureLog10(val1.value)+1)*sizeof(char))];
                        sprintf(str, "%f", val1.value);
                        char* str2 = &str[0];
                        if (isNegative) str2 = concat("-", str2);
                        result = concat(result, str2);
                    }
                    if (iterator->prev != nullptr)
                        result = concat(result, const_cast<char *>(", "));
                    iterator = iterator->prev;
                }
                result = concat(result, const_cast<char *>("}"));
                return result;
            } else {
                void* stringified = executeIntern(stringify->codes, stringify->loc, entriesDict);
                auto* stringValue = (StringValue*) stringified;
                textToPrint = concat(textToPrint, stringValue->value);
            }
        }
        if (i < array->used - 1)
            textToPrint = concat(textToPrint, const_cast<char *>(", "));
    }
    textToPrint = concat(textToPrint, const_cast<char *>("]"));
    return textToPrint;
}

void* routeAndResolve(char* funcRef, Dictionary* entries) {
    if (strcmp(funcRef, "print") == 0) {
        ConsolePrinter printer = createConsolePrinter();
        void* v = dict_get(entries, const_cast<char *>("text"));
        if (strcmp((*(Code*)v).type, "Object") == 0) {
            auto* obj = (Object*) v;
            printer.print(stringifyObject(obj));
        }
        else if (strcmp((*(Code*)v).type, "Array") == 0) {
            printer.print(stringifyArray((Array*) v));
        } else if ((*(Value*)v).valueType == 0x01) {
            printer.print((*(StringValue*)v).value);
        } else if ((*(Value*)v).valueType == 0x02) {
            ShortValue val1 = *(ShortValue*)v;
            char str[(int)(ceilf(measureLog10f(val1.value)+1)*sizeof(char))];
            sprintf(str, "%hd", val1.value);
            char* str2 = &str[0];
            printer.print(str2);
        }
        else if ((*(Value*)v).valueType == 0x06) {
            IntValue val1 = *(IntValue*)v;
            char str[(int)(ceil(measureLog10(val1.value)+1)*sizeof(char))];
            sprintf(str, "%d", val1.value);
            char* str2 = &str[0];
            printer.print(str2);
        }
        else if ((*(Value*)v).valueType == 0x03) {
            LongValue val1 = *(LongValue*)v;
            char str[(int)(ceill(measureLog10l(val1.value)+1)*sizeof(char))];
            sprintf(str, "%ld", val1.value);
            char* str2 = &str[0];
            printer.print(str2);
        }
        else if ((*(Value*)v).valueType == 0x04) {
            FloatValue val1 = *(FloatValue*)v;
            char str[(int)(ceilf(measureLog10f(val1.value)+1)*sizeof(char))];
            sprintf(str, "%f", val1.value);
            char* str2 = &str[0];
            printer.print(str2);
        }
        else if ((*(Value*)v).valueType == 0x05) {
            DoubleValue val1 = *(DoubleValue*)v;
            char str[(int)(ceil(measureLog10(val1.value)+1)*sizeof(char))];
            sprintf(str, "%f", val1.value);
            char* str2 = &str[0];
            printer.print(str2);
        }
        auto* value = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
        value->base.exp.base.type = const_cast<char *>("Value");
        value->base.exp.type = const_cast<char *>("Value");
        value->base.valueType = 0x01;
        value->value = const_cast<char *>("done.");
        return value;
    }
    else if (strcmp(funcRef, "sha256") == 0) {
        auto* result = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
        result->base.exp.base.type = const_cast<char *>("Value");
        result->base.exp.type = const_cast<char *>("Value");
        result->base.valueType = 0x01;
        char* value = sha256_hex((char*)((StringValue*)dict_get(entries, const_cast<char *>("content")))->value);
        result->value = value;
        return result;
    } else if (strcmp(funcRef, "len") == 0) {
        void* rawArr = dict_get(entries, const_cast<char *>("arr"));
        if (strcmp(((Code*)rawArr)->type, "Array") == 0) {
            auto* result = static_cast<IntValue *>(custom_alloc(sizeof(IntValue)));
            result->base.valueType = 0x06;
            result->base.exp.type = const_cast<char *>("Value");
            result->base.exp.base.type = const_cast<char *>("Value");
            result->value = static_cast<int>(((Array *) rawArr)->used);
            return result;
        }
    } else if (strcmp(funcRef, "time.now") == 0) {
        auto* result = static_cast<LongValue *>(custom_alloc(sizeof(LongValue)));
        result->base.valueType = 0x03;
        result->base.exp.type = const_cast<char *>("Value");
        result->base.exp.base.type = const_cast<char *>("Value");
        struct timeval time{};
        gettimeofday(&time, nullptr);
        int64_t s1 = (int64_t)(time.tv_sec) * 1000;
        int64_t s2 = (time.tv_usec / 1000);
        result->value = s1 + s2;
        return result;
    } else if (strcmp(funcRef, "append") == 0) {
        auto* array = (Array*) dict_get(entries, const_cast<char *>("list"));
        insertArray(array, dict_get(entries, const_cast<char *>("listItem")));
        auto* value = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
        value->base.exp.base.type = const_cast<char *>("Value");
        value->base.exp.type = const_cast<char *>("Value");
        value->base.valueType = 0x01;
        value->value = const_cast<char *>("done.");
        return value;
    } else if (strcmp(funcRef, "last") == 0) {
        auto* array = (Array*) dict_get(entries, const_cast<char *>("arr"));
        return array->array[array->used - 1];
    } else if (strcmp(funcRef, "stringify") == 0) {
        void* obj = dict_get(entries, const_cast<char *>("obj"));
        if (strcmp(((Code*)obj)->type, "Array") == 0) {
            auto* result = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
            result->base.exp.base.type = const_cast<char *>("Value");
            result->base.exp.type = const_cast<char *>("Value");
            result->base.valueType = 0x01;
            char* resValue = stringifyArray((Array*)obj);
            result->value = resValue;
            return result;
        }
        else if (strcmp(((Code*)obj)->type, "Object") == 0) {
            auto* result = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
            result->base.exp.base.type = const_cast<char *>("Value");
            result->base.exp.type = const_cast<char *>("Value");
            result->base.valueType = 0x01;
            result->value = stringifyObject((Object*)obj);
            return result;
        } else if ((*(Value*)obj).valueType == 0x02) {
            ShortValue val1 = *(ShortValue*)obj;
            char str[(int)(ceilf(measureLog10f(val1.value)+1)*sizeof(char))];
            sprintf(str, "%hd", val1.value);
            char* str2 = &str[0];
            auto* result = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
            result->base.exp.base.type = const_cast<char *>("Value");
            result->base.exp.type = const_cast<char *>("Value");
            result->base.valueType = 0x01;
            result->value = str2;
            return result;
        }
        else if ((*(Value*)obj).valueType == 0x06) {
            IntValue val1 = *(IntValue*)obj;
            char str[(int)(ceil(measureLog10(val1.value)+1)*sizeof(char))];
            sprintf(str, "%d", val1.value);
            char* str2 = &str[0];
            auto* result = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
            result->base.exp.base.type = const_cast<char *>("Value");
            result->base.exp.type = const_cast<char *>("Value");
            result->base.valueType = 0x01;
            result->value = str2;
            return result;
        }
        else if ((*(Value*)obj).valueType == 0x03) {
            LongValue val1 = *(LongValue*)obj;
            char str[(int)(ceill(measureLog10l(val1.value)+1)*sizeof(char))];
            sprintf(str, "%ld", val1.value);
            char* str2 = &str[0];
            auto* result = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
            result->base.exp.base.type = const_cast<char *>("Value");
            result->base.exp.type = const_cast<char *>("Value");
            result->base.valueType = 0x01;
            result->value = str2;
            return result;
        }
        else if ((*(Value*)obj).valueType == 0x04) {
            FloatValue val1 = *(FloatValue*)obj;
            char str[(int)(ceilf(measureLog10f(val1.value)+1)*sizeof(char))];
            sprintf(str, "%f", val1.value);
            char* str2 = &str[0];
            auto* result = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
            result->base.exp.base.type = const_cast<char *>("Value");
            result->base.exp.type = const_cast<char *>("Value");
            result->base.valueType = 0x01;
            result->value = str2;
            return result;
        }
        else if ((*(Value*)obj).valueType == 0x05) {
            DoubleValue val1 = *(DoubleValue*)obj;
            char str[(int)(ceil(measureLog10(val1.value)+1)*sizeof(char))];
            sprintf(str, "%f", val1.value);
            char* str2 = &str[0];
            auto* result = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
            result->base.exp.base.type = const_cast<char *>("Value");
            result->base.exp.type = const_cast<char *>("Value");
            result->base.valueType = 0x01;
            result->value = str2;
            return result;
        } else if ((*(Value*)obj).valueType == 0x01) {
            return obj;
        }
    } else if (strcmp(funcRef, "uuid") == 0) {
        auto* result = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
        result->base.exp.base.type = const_cast<char *>("Value");
        result->base.exp.type = const_cast<char *>("Value");
        result->base.valueType = 0x01;
        result->value = generate_uuid();
        return result;
    } else if (strcmp(funcRef, "replace") == 0) {
        auto* str = (StringValue*)dict_get(entries, const_cast<char *>("str"));
        auto* from = (StringValue*)dict_get(entries, const_cast<char *>("from"));
        auto* to = (StringValue*)dict_get(entries, const_cast<char *>("to"));
        char* resultStr = static_cast<char *>(custom_alloc(strlen(str->value)));
        strcpy(resultStr, str->value);
        replaceWord(resultStr, from->value, to->value);
        auto* result = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
        result->base.exp.base.type = const_cast<char *>("Value");
        result->base.exp.type = const_cast<char *>("Value");
        result->base.valueType = 0x01;
        result->value = resultStr;
        return result;
    } else if (strcmp(funcRef, "match_object_structure") == 0) {
        auto* obj = (Object*) dict_get(entries, const_cast<char *>("obj"));
        auto* schema = (Array*) dict_get(entries, const_cast<char *>("structure"));
        for (unsigned long i = 0; i < schema->used; i++) {
            auto* fieldName = (StringValue*) schema->array[i];
            if (dict_get(obj->value, fieldName->value) == nullptr) {
                auto* result = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
                result->base.exp.base.type = const_cast<char *>("Value");
                result->base.exp.type = const_cast<char *>("Value");
                result->base.valueType = 0x07;
                result->value = false;
                return result;
            }
        }
        auto* result = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
        result->base.exp.base.type = const_cast<char *>("Value");
        result->base.exp.type = const_cast<char *>("Value");
        result->base.valueType = 0x07;
        result->value = true;
        return result;
    }
    return nullptr;
}

void* sum(void* value1, void* value2) {
    Value val1Raw = *(Value*)value1;
    Value val2Raw = *(Value*)value2;
    if (val1Raw.valueType == 0x06 ||
        val1Raw.valueType == 0x02 ||
        val1Raw.valueType == 0x03 ||
        val1Raw.valueType == 0x04 ||
        val1Raw.valueType == 0x05) {
        if (val2Raw.valueType == 0x06 ||
            val2Raw.valueType == 0x02 ||
            val2Raw.valueType == 0x03 ||
            val2Raw.valueType == 0x04 ||
            val2Raw.valueType == 0x05) {
            long double result = 0;
            if (val1Raw.valueType == 0x02) {
                ShortValue val1 = *(ShortValue*)value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue*)value2;
                    result = (long double)val1.value + (long double) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue*)value2;
                    result = (long double)val1.value + (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                }
            } else if (val1Raw.valueType == 0x06) {
                IntValue val1 = *(IntValue*)value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue*)value2;
                    result = (long double)val1.value + (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                }
            } else if (val1Raw.valueType == 0x03) {
                LongValue val1 = *(LongValue*)value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue*)value2;
                    result = (long double) val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue*)value2;
                    result = (long double) val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue*)value2;
                    result = (long double) val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue*)value2;
                    result = (long double) val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue*)value2;
                    result = (long double) val1.value + (long double)val2.value;
                }
            } else if (val1Raw.valueType == 0x04) {
                FloatValue val1 = *(FloatValue*)value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                }
            } else if (val1Raw.valueType == 0x05) {
                DoubleValue val1 = *(DoubleValue*)value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue*)value2;
                    result = (long double)val1.value + (long double)val2.value;
                }
            }
            if (floorl(result) == result) {
                if (result < INT16_MAX) {
                    auto* resValue = static_cast<ShortValue *>(custom_alloc(sizeof(ShortValue)));
                    auto r = (short)result;
                    resValue->value = r;
                    resValue->base.valueType = 0x02;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
                else if (result < INT32_MAX) {
                    auto* resValue = static_cast<IntValue *>(custom_alloc(sizeof(IntValue)));
                    int r = (int)result;
                    resValue->value = r;
                    resValue->base.valueType = 0x06;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
                else if (result < INT64_MAX) {
                    auto* resValue = static_cast<LongValue *>(custom_alloc(sizeof(LongValue)));
                    long r = (long)result;
                    resValue->value = r;
                    resValue->base.valueType = 0x03;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            }
            else {
                if (result < FLT_MAX) {
                    auto* resValue = static_cast<FloatValue *>(custom_alloc(sizeof(FloatValue)));
                    auto r = (float)result;
                    resValue->value = r;
                    resValue->base.valueType = 0x04;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
                else if (result < DBL_MAX) {
                    auto* resValue = static_cast<DoubleValue *>(custom_alloc(sizeof(DoubleValue)));
                    auto r = (double)result;
                    resValue->value = r;
                    resValue->base.valueType = 0x05;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            }
        }
        else if (val2Raw.valueType == 0x01) {
            StringValue val2 = *(StringValue*)value2;
            char* str2 = const_cast<char *>("");
            if (val1Raw.valueType == 0x02) {
                ShortValue val1 = *(ShortValue*)value1;
                char str[(int)(ceil(measureLog10(val1.value)+1)*sizeof(char))];
                sprintf(str, "%hd", val1.value);
                str2 = static_cast<char *>(custom_alloc(strlen(str)));
                strcpy(str2, str);
            } else if (val1Raw.valueType == 0x06) {
                IntValue val1 = *(IntValue*)value1;
                char str[(int)(ceil(measureLog10(val1.value)+1)*sizeof(char))];
                sprintf(str, "%d", val1.value);
                str2 = static_cast<char *>(custom_alloc(strlen(str)));
                strcpy(str2, str);
            } else if (val1Raw.valueType == 0x03) {
                LongValue val1 = *(LongValue*)value1;
                char str[(int)(ceill(measureLog10l(val1.value)+1)*sizeof(char))];
                sprintf(str, "%ld", val1.value);
                str2 = static_cast<char *>(custom_alloc(strlen(str)));
                strcpy(str2, str);
            } else if (val1Raw.valueType == 0x04) {
                FloatValue val1 = *(FloatValue*)value1;
                char str[(int)(ceilf(measureLog10f(val1.value)+1)*sizeof(char))];
                sprintf(str, "%f", val1.value);
                str2 = static_cast<char *>(custom_alloc(strlen(str)));
                strcpy(str2, str);
            } else if (val1Raw.valueType == 0x05) {
                DoubleValue val1 = *(DoubleValue*)value1;
                char str[(int)(ceil(measureLog10(val1.value)+1)*sizeof(char))];
                sprintf(str, "%f", val1.value);
                str2 = static_cast<char *>(custom_alloc(strlen(str)));
                strcpy(str2, str);
            }
            auto* resValue = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
            char* result = concat(str2, val2.value);
            resValue->value = result;
            resValue->base.valueType = 0x01;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
        else if (val2Raw.valueType == 0x07) {
            StringValue val2 = *(StringValue*)value2;
            double result = 0;
            if (val1Raw.valueType == 0x02) {
                ShortValue val1 = *(ShortValue*)value1;
                result = (double)val1.value + (val2.value ? 1 : 0);
            } else if (val1Raw.valueType == 0x06) {
                IntValue val1 = *(IntValue*)value1;
                result = (double)val1.value + (val2.value ? 1 : 0);
            } else if (val1Raw.valueType == 0x03) {
                LongValue val1 = *(LongValue*)value1;
                result = (double)val1.value + (val2.value ? 1 : 0);
            } else if (val1Raw.valueType == 0x04) {
                FloatValue val1 = *(FloatValue*)value1;
                result = (double)val1.value + (val2.value ? 1 : 0);
            } else if (val1Raw.valueType == 0x05) {
                DoubleValue val1 = *(DoubleValue*)value1;
                result = (double)val1.value + (val2.value ? 1 : 0);
            }
            if (floor(result) == result) {
                if (result < INT16_MAX) {
                    auto* resValue = static_cast<ShortValue *>(custom_alloc(sizeof(ShortValue)));
                    auto r = (short)result;
                    resValue->value = r;
                    resValue->base.valueType = 0x02;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
                else if (result < INT32_MAX) {
                    auto* resValue = static_cast<IntValue *>(custom_alloc(sizeof(IntValue)));
                    int r = (int)result;
                    resValue->value = r;
                    resValue->base.valueType = 0x06;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
                else if (result < INT64_MAX) {
                    auto* resValue = static_cast<LongValue *>(custom_alloc(sizeof(LongValue)));
                    long r = (long)result;
                    resValue->value = r;
                    resValue->base.valueType = 0x03;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            }
            else {
                if (result < FLT_MAX) {
                    auto* resValue = static_cast<FloatValue *>(custom_alloc(sizeof(FloatValue)));
                    auto r = (float)result;
                    resValue->value = r;
                    resValue->base.valueType = 0x06;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
                else if (result < DBL_MAX) {
                    auto* resValue = static_cast<DoubleValue *>(custom_alloc(sizeof(DoubleValue)));
                    auto r = (double)result;
                    resValue->value = r;
                    resValue->base.valueType = 0x06;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            }
        }
    }
    else if (val1Raw.valueType == 0x01) {
        StringValue val1 = *(StringValue*)value1;
        if (val2Raw.valueType == 0x06 ||
            val2Raw.valueType == 0x02 ||
            val2Raw.valueType == 0x03 ||
            val2Raw.valueType == 0x04 ||
            val2Raw.valueType == 0x05) {
            char* str2 = const_cast<char *>("");
            if (val2Raw.valueType == 0x02) {
                ShortValue val2 = *(ShortValue*)value2;
                char str[(int)(ceil(measureLog10(val2.value)+1)*sizeof(char))];
                sprintf(str, "%hd", val2.value);
                str2 = static_cast<char *>(custom_alloc(strlen(str)));
                strcpy(str2, str);
            } else if (val2Raw.valueType == 0x06) {
                IntValue val2 = *(IntValue*)value2;
                char str[(int)(ceil(measureLog10(val2.value)+1)*sizeof(char))];
                sprintf(str, "%d", val2.value);
                str2 = static_cast<char *>(custom_alloc(strlen(str)));
                strcpy(str2, str);
            } else if (val2Raw.valueType == 0x03) {
                LongValue val2 = *(LongValue*)value2;
                char str[(int)(ceill(measureLog10l(val2.value)+1)*sizeof(char))];
                sprintf(str, "%ld", val2.value);
                str2 = static_cast<char *>(custom_alloc(strlen(str)));
                strcpy(str2, str);
            } else if (val2Raw.valueType == 0x04) {
                FloatValue val2 = *(FloatValue*)value2;
                char str[(int)(ceilf(measureLog10f(val2.value)+1)*sizeof(char))];
                sprintf(str, "%f", val2.value);
                str2 = static_cast<char *>(custom_alloc(strlen(str)));
                strcpy(str2, str);
            } else if (val2Raw.valueType == 0x05) {
                DoubleValue val2 = *(DoubleValue*)value2;
                char str[(int)(ceil(measureLog10(val2.value)+1)*sizeof(char))];
                sprintf(str, "%f", val2.value);
                str2 = static_cast<char *>(custom_alloc(strlen(str)));
                strcpy(str2, str);
            }
            auto* resValue = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
            char* dest = concat(val1.value, str2);
            resValue->value = dest;
            resValue->base.valueType = 0x01;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        } else if (val2Raw.valueType == 0x01) {
            StringValue val2 = *(StringValue*)value2;
            char* result = concat(val1.value, val2.value);
            auto* resValue = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
            resValue->value = result;
            resValue->base.valueType = 0x01;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        } else if (val2Raw.valueType == 0x07) {
            BoolValue val2 = *(BoolValue*)value2;
            char* boolStr = const_cast<char *>(val2.value ? "true" : "false");
            char* result = concat(val1.value, boolStr);
            auto* resValue = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
            resValue->value = result;
            resValue->base.valueType = 0x01;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    else if (val1Raw.valueType == 0x07) {
        BoolValue val1 = *(BoolValue*)value1;
        if (val2Raw.valueType == 0x06 ||
            val2Raw.valueType == 0x02 ||
            val2Raw.valueType == 0x03 ||
            val2Raw.valueType == 0x04 ||
            val2Raw.valueType == 0x05) {
            double result = 0;
            if (val2Raw.valueType == 0x02) {
                ShortValue val2 = *(ShortValue*)value2;
                result = (double)(val1.value ? 1 : 0) + (double)val2.value;
            } else if (val2Raw.valueType == 0x06) {
                IntValue val2 = *(IntValue*)value2;
                result = (double)(val1.value ? 1 : 0) + (double)val2.value;
            } else if (val2Raw.valueType == 0x03) {
                LongValue val2 = *(LongValue*)value2;
                result = (double)(val1.value ? 1 : 0) + (double)val2.value;
            } else if (val2Raw.valueType == 0x04) {
                FloatValue val2 = *(FloatValue*)value2;
                result = (double)(val1.value ? 1 : 0) + (double)val2.value;
            } else if (val2Raw.valueType == 0x05) {
                DoubleValue val2 = *(DoubleValue*)value2;
                result = (double)(val1.value ? 1 : 0) + (double)val2.value;
            }
            if (floor(result) == result) {
                if (result < INT16_MAX) {
                    auto r = (short)result;
                    auto* resValue = static_cast<ShortValue *>(custom_alloc(sizeof(ShortValue)));
                    resValue->value = r;
                    resValue->base.valueType = 0x02;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
                else if (result < INT32_MAX) {
                    int r = (int)result;
                    auto* resValue = static_cast<IntValue *>(custom_alloc(sizeof(IntValue)));
                    resValue->value = r;
                    resValue->base.valueType = 0x06;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
                else if (result < INT64_MAX) {
                    long r = (long)result;
                    auto* resValue = static_cast<LongValue *>(custom_alloc(sizeof(LongValue)));
                    resValue->value = r;
                    resValue->base.valueType = 0x03;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            }
            else {
                if (result < FLT_MAX) {
                    auto r = (float)result;
                    auto* resValue = static_cast<FloatValue *>(custom_alloc(sizeof(FloatValue)));
                    resValue->value = r;
                    resValue->base.valueType = 0x04;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
                else if (result < DBL_MAX) {
                    auto r = (double)result;
                    auto* resValue = static_cast<DoubleValue *>(custom_alloc(sizeof(DoubleValue)));
                    resValue->value = r;
                    resValue->base.valueType = 0x05;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            }
        } else if (val2Raw.valueType == 0x01) {
            StringValue val2 = *(StringValue*)value2;
            char* result = concat(val1.value ? "true" : "false", val2.value);
            auto* resValue = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
            resValue->value = result;
            resValue->base.valueType = 0x01;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        } else if (val2Raw.valueType == 0x07) {
            BoolValue val2 = *(BoolValue*)value2;
            bool result = val1.value || val2.value;
            auto* resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    return nullptr;
}

void* subtract(void* value1, void* value2) {
    Value val1Raw = *(Value*)value1;
    Value val2Raw = *(Value*)value2;
    if (val1Raw.valueType == 0x06 ||
        val1Raw.valueType == 0x02 ||
        val1Raw.valueType == 0x03 ||
        val1Raw.valueType == 0x04 ||
        val1Raw.valueType == 0x05) {
        if (val2Raw.valueType == 0x06 ||
            val2Raw.valueType == 0x02 ||
            val2Raw.valueType == 0x03 ||
            val2Raw.valueType == 0x04 ||
            val2Raw.valueType == 0x05) {
            long double result = 0;
            if (val1Raw.valueType == 0x02) {
                ShortValue val1 = *(ShortValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                }
            } else if (val1Raw.valueType == 0x06) {
                IntValue val1 = *(IntValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                }
            } else if (val1Raw.valueType == 0x03) {
                LongValue val1 = *(LongValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                }
            } else if (val1Raw.valueType == 0x04) {
                FloatValue val1 = *(FloatValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                }
            } else if (val1Raw.valueType == 0x05) {
                DoubleValue val1 = *(DoubleValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value - (long double) val2.value;
                }
            }
            if (floorl(result) == result) {
                if (result < INT16_MAX) {
                    auto *resValue = static_cast<ShortValue *>(custom_alloc(sizeof(ShortValue)));
                    auto r = (short) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x02;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < INT32_MAX) {
                    auto *resValue = static_cast<IntValue *>(custom_alloc(sizeof(IntValue)));
                    int r = (int) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x06;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < INT64_MAX) {
                    auto *resValue = static_cast<LongValue *>(custom_alloc(sizeof(LongValue)));
                    long r = (long) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x03;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            } else {
                if (result < FLT_MAX) {
                    auto *resValue = static_cast<FloatValue *>(custom_alloc(sizeof(FloatValue)));
                    auto r = (float) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x04;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < DBL_MAX) {
                    auto *resValue = static_cast<DoubleValue *>(custom_alloc(sizeof(DoubleValue)));
                    auto r = (double) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x05;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            }
        }
    }
    return nullptr;
}

void* multiply(void* value1, void* value2) {
    Value val1Raw = *(Value*)value1;
    Value val2Raw = *(Value*)value2;
    if (val1Raw.valueType == 0x06 ||
        val1Raw.valueType == 0x02 ||
        val1Raw.valueType == 0x03 ||
        val1Raw.valueType == 0x04 ||
        val1Raw.valueType == 0x05) {
        if (val2Raw.valueType == 0x06 ||
            val2Raw.valueType == 0x02 ||
            val2Raw.valueType == 0x03 ||
            val2Raw.valueType == 0x04 ||
            val2Raw.valueType == 0x05) {
            long double result = 0;
            if (val1Raw.valueType == 0x02) {
                ShortValue val1 = *(ShortValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                }
            } else if (val1Raw.valueType == 0x06) {
                IntValue val1 = *(IntValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                }
            } else if (val1Raw.valueType == 0x03) {
                LongValue val1 = *(LongValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                }
            } else if (val1Raw.valueType == 0x04) {
                FloatValue val1 = *(FloatValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                }
            } else if (val1Raw.valueType == 0x05) {
                DoubleValue val1 = *(DoubleValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value * (long double) val2.value;
                }
            }
            if (floorl(result) == result) {
                if (result < INT16_MAX) {
                    auto *resValue = static_cast<ShortValue *>(custom_alloc(sizeof(ShortValue)));
                    auto r = (short) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x02;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < INT32_MAX) {
                    auto *resValue = static_cast<IntValue *>(custom_alloc(sizeof(IntValue)));
                    int r = (int) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x06;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < INT64_MAX) {
                    auto *resValue = static_cast<LongValue *>(custom_alloc(sizeof(LongValue)));
                    long r = (long) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x03;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            } else {
                if (result < FLT_MAX) {
                    auto *resValue = static_cast<FloatValue *>(custom_alloc(sizeof(FloatValue)));
                    auto r = (float) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x04;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < DBL_MAX) {
                    auto *resValue = static_cast<DoubleValue *>(custom_alloc(sizeof(DoubleValue)));
                    auto r = (double) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x05;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            }
        }
    }
    return nullptr;
}

void* divide(void* value1, void* value2) {
    Value val1Raw = *(Value*)value1;
    Value val2Raw = *(Value*)value2;
    if (val1Raw.valueType == 0x06 ||
        val1Raw.valueType == 0x02 ||
        val1Raw.valueType == 0x03 ||
        val1Raw.valueType == 0x04 ||
        val1Raw.valueType == 0x05) {
        if (val2Raw.valueType == 0x06 ||
            val2Raw.valueType == 0x02 ||
            val2Raw.valueType == 0x03 ||
            val2Raw.valueType == 0x04 ||
            val2Raw.valueType == 0x05) {
            long double result = 0;
            if (val1Raw.valueType == 0x02) {
                ShortValue val1 = *(ShortValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                }
            } else if (val1Raw.valueType == 0x06) {
                IntValue val1 = *(IntValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                }
            } else if (val1Raw.valueType == 0x03) {
                LongValue val1 = *(LongValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                }
            } else if (val1Raw.valueType == 0x04) {
                FloatValue val1 = *(FloatValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                }
            } else if (val1Raw.valueType == 0x05) {
                DoubleValue val1 = *(DoubleValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                } else if (val2Raw.valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value / (long double) val2.value;
                }
            }
            if (floorl(result) == result) {
                if (result < INT16_MAX) {
                    auto *resValue = static_cast<ShortValue *>(custom_alloc(sizeof(ShortValue)));
                    auto r = (short) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x02;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < INT32_MAX) {
                    auto *resValue = static_cast<IntValue *>(custom_alloc(sizeof(IntValue)));
                    int r = (int) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x06;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < INT64_MAX) {
                    auto *resValue = static_cast<LongValue *>(custom_alloc(sizeof(LongValue)));
                    long r = (long) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x03;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            } else {
                if (result < FLT_MAX) {
                    auto *resValue = static_cast<FloatValue *>(custom_alloc(sizeof(FloatValue)));
                    auto r = (float) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x04;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < DBL_MAX) {
                    auto *resValue = static_cast<DoubleValue *>(custom_alloc(sizeof(DoubleValue)));
                    auto r = (double) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x05;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            }
        }
    }
    return nullptr;
}

void* mod(void* value1, void* value2) {
    Value val1Raw = *(Value*)value1;
    Value val2Raw = *(Value*)value2;
    if (val1Raw.valueType == 0x06 ||
        val1Raw.valueType == 0x02 ||
        val1Raw.valueType == 0x03 ||
        val1Raw.valueType == 0x04 ||
        val1Raw.valueType == 0x05) {
        if (val2Raw.valueType == 0x06 ||
            val2Raw.valueType == 0x02 ||
            val2Raw.valueType == 0x03 ||
            val2Raw.valueType == 0x04 ||
            val2Raw.valueType == 0x05) {
            long double result = 0;
            if (val1Raw.valueType == 0x02) {
                ShortValue val1 = *(ShortValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long) val1.value % (long) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long) val1.value % (long) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long) val1.value % (long) val2.value;
                }
            } else if (val1Raw.valueType == 0x06) {
                IntValue val1 = *(IntValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long) val1.value % (long) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long) val1.value % (long) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long) val1.value % (long) val2.value;
                }
            } else if (val1Raw.valueType == 0x03) {
                LongValue val1 = *(LongValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long) val1.value % (long) val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long) val1.value % (long) val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long) val1.value % (long) val2.value;
                }
            }
            if (floorl(result) == result) {
                if (result < INT16_MAX) {
                    auto *resValue = static_cast<ShortValue *>(custom_alloc(sizeof(ShortValue)));
                    auto r = (short) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x02;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < INT32_MAX) {
                    auto *resValue = static_cast<IntValue *>(custom_alloc(sizeof(IntValue)));
                    int r = (int) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x06;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < INT64_MAX) {
                    auto *resValue = static_cast<LongValue *>(custom_alloc(sizeof(LongValue)));
                    long r = (long) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x03;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            }
        }
    }
    return nullptr;
}

void* power(void* value1, void* value2) {
    auto* val1Raw = (Value*)value1;
    auto* val2Raw = (Value*)value2;
    if (val1Raw->valueType == 0x06 ||
        val1Raw->valueType == 0x02 ||
        val1Raw->valueType == 0x03 ||
        val1Raw->valueType == 0x04 ||
        val1Raw->valueType == 0x05) {
        if (val2Raw->valueType == 0x06 ||
            val2Raw->valueType == 0x02 ||
            val2Raw->valueType == 0x03 ||
            val2Raw->valueType == 0x04 ||
            val2Raw->valueType == 0x05) {
            long double result = false;
            if (val1Raw->valueType == 0x02) {
                ShortValue val1 = *(ShortValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                }
            } else if (val1Raw->valueType == 0x06) {
                auto* val1 = (IntValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    auto* val2 = (ShortValue *) value2;
                    result = powl((long double) val1->value, (long double) val2->value);
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = powl((long double) val1->value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = powl((long double) val1->value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = powl((long double) val1->value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = powl((long double) val1->value, (long double) val2.value);
                }
            } else if (val1Raw->valueType == 0x03) {
                LongValue val1 = *(LongValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                }
            } else if (val1Raw->valueType == 0x04) {
                FloatValue val1 = *(FloatValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                }
            } else if (val1Raw->valueType == 0x05) {
                DoubleValue val1 = *(DoubleValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = powl((long double) val1.value, (long double) val2.value);
                }
            }
            if (floorl(result) == result) {
                if (result < INT16_MAX) {
                    auto *resValue = static_cast<ShortValue *>(custom_alloc(sizeof(ShortValue)));
                    auto r = (short) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x02;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < INT32_MAX) {
                    auto *resValue = static_cast<IntValue *>(custom_alloc(sizeof(IntValue)));
                    int r = (int) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x06;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < INT64_MAX) {
                    auto *resValue = static_cast<LongValue *>(custom_alloc(sizeof(LongValue)));
                    long r = (long) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x03;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            } else {
                if (result < FLT_MAX) {
                    auto *resValue = static_cast<FloatValue *>(custom_alloc(sizeof(FloatValue)));
                    auto r = (float) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x04;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < DBL_MAX) {
                    auto *resValue = static_cast<DoubleValue *>(custom_alloc(sizeof(DoubleValue)));
                    auto r = (double) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x05;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            }
        }
    }
    return nullptr;
}

void* andOp(void* value1, void* value2) {
    Value val1Raw = *(Value*)value1;
    Value val2Raw = *(Value*)value2;
    if (value1 == nullptr || value2 == nullptr) {
        auto* result = static_cast<LongValue *>(custom_alloc(sizeof(LongValue)));
        result->base.valueType = 0x03;
        result->base.exp.type = const_cast<char *>("Value");
        result->base.exp.base.type = const_cast<char *>("Value");
        result->value = 0;
        return value2;
    }
    if (val1Raw.valueType == 0x06 ||
        val1Raw.valueType == 0x02 ||
        val1Raw.valueType == 0x03 ||
        val1Raw.valueType == 0x04 ||
        val1Raw.valueType == 0x05) {
        if (val2Raw.valueType == 0x06 ||
            val2Raw.valueType == 0x02 ||
            val2Raw.valueType == 0x03 ||
            val2Raw.valueType == 0x04 ||
            val2Raw.valueType == 0x05) {
            long double result = 0;
            if (val1Raw.valueType == 0x02) {
                ShortValue val1 = *(ShortValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = val1.value & val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = val1.value & val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = val1.value & val2.value;
                }
            } else if (val1Raw.valueType == 0x06) {
                IntValue val1 = *(IntValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = val1.value & val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = val1.value & val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = val1.value & val2.value;
                }
            } else if (val1Raw.valueType == 0x03) {
                LongValue val1 = *(LongValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = val1.value & val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = val1.value & val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = val1.value & val2.value;
                }
            }
            if (floorl(result) == result) {
                if (result < INT16_MAX) {
                    auto *resValue = static_cast<ShortValue *>(custom_alloc(sizeof(ShortValue)));
                    auto r = (short) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x02;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < INT32_MAX) {
                    auto *resValue = static_cast<IntValue *>(custom_alloc(sizeof(IntValue)));
                    int r = (int) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x06;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < INT64_MAX) {
                    auto *resValue = static_cast<LongValue *>(custom_alloc(sizeof(LongValue)));
                    long r = (long) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x03;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            } else {
                if (result < FLT_MAX) {
                    auto *resValue = static_cast<FloatValue *>(custom_alloc(sizeof(FloatValue)));
                    auto r = (float) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x04;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < DBL_MAX) {
                    auto *resValue = static_cast<DoubleValue *>(custom_alloc(sizeof(DoubleValue)));
                    auto r = (double) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x05;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            }
        }
    }
    else if (val1Raw.valueType == 0x07) {
        auto* val1 = (BoolValue*) value1;
        if (val2Raw.valueType == 0x07) {
            auto* val2 = (BoolValue*) value2;
            bool result = val1->value & val2->value;
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            return resValue;
        }
    }
    return nullptr;
}

void* orOp(void* value1, void* value2) {
    if (value1 == nullptr || strcmp(((Code*)value1)->type, "Empty") == 0) {
        return value2;
    }
    else if (value2 == nullptr || strcmp(((Code*)value2)->type, "Empty") == 0) {
        return value1;
    }
    Value val1Raw = *(Value*)value1;
    Value val2Raw = *(Value*)value2;
    if (val1Raw.valueType == 0x06 ||
        val1Raw.valueType == 0x02 ||
        val1Raw.valueType == 0x03 ||
        val1Raw.valueType == 0x04 ||
        val1Raw.valueType == 0x05) {
        if (val2Raw.valueType == 0x06 ||
            val2Raw.valueType == 0x02 ||
            val2Raw.valueType == 0x03 ||
            val2Raw.valueType == 0x04 ||
            val2Raw.valueType == 0x05) {
            long double result = 0;
            if (val1Raw.valueType == 0x02) {
                ShortValue val1 = *(ShortValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = val1.value | val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = val1.value | val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = val1.value | val2.value;
                }
            } else if (val1Raw.valueType == 0x06) {
                IntValue val1 = *(IntValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = val1.value | val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = val1.value | val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = val1.value | val2.value;
                }
            } else if (val1Raw.valueType == 0x03) {
                LongValue val1 = *(LongValue *) value1;
                if (val2Raw.valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = val1.value | val2.value;
                } else if (val2Raw.valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = val1.value | val2.value;
                } else if (val2Raw.valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = val1.value | val2.value;
                }
            }
            if (floorl(result) == result) {
                if (result < INT16_MAX) {
                    auto *resValue = static_cast<ShortValue *>(custom_alloc(sizeof(ShortValue)));
                    auto r = (short) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x02;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < INT32_MAX) {
                    auto *resValue = static_cast<IntValue *>(custom_alloc(sizeof(IntValue)));
                    int r = (int) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x06;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < INT64_MAX) {
                    auto *resValue = static_cast<LongValue *>(custom_alloc(sizeof(LongValue)));
                    long r = (long) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x03;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            } else {
                if (result < FLT_MAX) {
                    auto *resValue = static_cast<FloatValue *>(custom_alloc(sizeof(FloatValue)));
                    auto r = (float) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x04;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                } else if (result < DBL_MAX) {
                    auto *resValue = static_cast<DoubleValue *>(custom_alloc(sizeof(DoubleValue)));
                    auto r = (double) result;
                    resValue->value = r;
                    resValue->base.valueType = 0x05;
                    resValue->base.exp.base.type = const_cast<char *>("Value");
                    resValue->base.exp.type = const_cast<char *>("Value");
                    return resValue;
                }
            }
        }
    }
    else if (val1Raw.valueType == 0x07) {
        auto* val1 = (BoolValue*) value1;
        if (val2Raw.valueType == 0x07) {
            auto* val2 = (BoolValue*) value2;
            bool result = val1->value | val2->value;
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            return resValue;
        }
    }
    return nullptr;
}

void* equal(void* value1, void* value2) {
    auto* val1Raw = (Value*)value1;
    auto* val2Raw = (Value*)value2;
    if (val1Raw->valueType == 0x06 ||
        val1Raw->valueType == 0x02 ||
        val1Raw->valueType == 0x03 ||
        val1Raw->valueType == 0x04 ||
        val1Raw->valueType == 0x05) {
        if (val2Raw->valueType == 0x06 ||
            val2Raw->valueType == 0x02 ||
            val2Raw->valueType == 0x03 ||
            val2Raw->valueType == 0x04 ||
            val2Raw->valueType == 0x05) {
            bool result = false;
            if (val1Raw->valueType == 0x02) {
                ShortValue val1 = *(ShortValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x06) {
                IntValue val1 = *(IntValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x03) {
                LongValue val1 = *(LongValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) *(long *) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) *(long *) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) *(long *) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) *(long *) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) *(long *) val1.value == (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x04) {
                FloatValue val1 = *(FloatValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x05) {
                DoubleValue val1 = *(DoubleValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value == (long double) val2.value;
                }
            }
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    else if (val1Raw->valueType == 0x01) {
        auto* val1 = (StringValue*) value1;
        if (val2Raw->valueType == 0x01) {
            auto* val2 = (StringValue*) value2;
            bool result = strcmp(val1->value, val2->value) == 0;
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    else if (val1Raw->valueType == 0x07) {
        auto* val1 = (BoolValue*) value1;
        if (val2Raw->valueType == 0x07) {
            auto* val2 = (BoolValue*) value2;
            bool result = val1->value == val2->value;
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
    resValue->value = false;
    resValue->base.valueType = 0x07;
    resValue->base.exp.base.type = const_cast<char *>("Value");
    resValue->base.exp.type = const_cast<char *>("Value");
    return resValue;
}

void* ne(void* value1, void* value2) {
    auto* val1Raw = (Value*)value1;
    auto* val2Raw = (Value*)value2;
    if (val1Raw->valueType == 0x06 ||
        val1Raw->valueType == 0x02 ||
        val1Raw->valueType == 0x03 ||
        val1Raw->valueType == 0x04 ||
        val1Raw->valueType == 0x05) {
        if (val2Raw->valueType == 0x06 ||
            val2Raw->valueType == 0x02 ||
            val2Raw->valueType == 0x03 ||
            val2Raw->valueType == 0x04 ||
            val2Raw->valueType == 0x05) {
            bool result = false;
            if (val1Raw->valueType == 0x02) {
                ShortValue val1 = *(ShortValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x06) {
                IntValue val1 = *(IntValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x03) {
                LongValue val1 = *(LongValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) *(long *) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) *(long *) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) *(long *) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) *(long *) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) *(long *) val1.value != (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x04) {
                FloatValue val1 = *(FloatValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x05) {
                DoubleValue val1 = *(DoubleValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value != (long double) val2.value;
                }
            }
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    else if (val1Raw->valueType == 0x01) {
        auto* val1 = (StringValue*) value1;
        if (val2Raw->valueType == 0x01) {
            auto* val2 = (StringValue*) value2;
            bool result = strcmp(val1->value, val2->value) != 0;
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    else if (val1Raw->valueType == 0x07) {
        auto* val1 = (BoolValue*) value1;
        if (val2Raw->valueType == 0x07) {
            auto* val2 = (BoolValue*) value2;
            bool result = val1->value != val2->value;
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
    resValue->value = false;
    resValue->base.valueType = 0x07;
    resValue->base.exp.base.type = const_cast<char *>("Value");
    resValue->base.exp.type = const_cast<char *>("Value");
    return resValue;
}

void* lt(void* value1, void* value2) {
    auto* val1Raw = (Value*)value1;
    auto* val2Raw = (Value*)value2;
    if (val1Raw->valueType == 0x06 ||
        val1Raw->valueType == 0x02 ||
        val1Raw->valueType == 0x03 ||
        val1Raw->valueType == 0x04 ||
        val1Raw->valueType == 0x05) {
        if (val2Raw->valueType == 0x06 ||
            val2Raw->valueType == 0x02 ||
            val2Raw->valueType == 0x03 ||
            val2Raw->valueType == 0x04 ||
            val2Raw->valueType == 0x05) {
            bool result = false;
            if (val1Raw->valueType == 0x02) {
                ShortValue val1 = *(ShortValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x06) {
                IntValue val1 = *(IntValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x03) {
                LongValue val1 = *(LongValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) *(long *) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) *(long *) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) *(long *) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) *(long *) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) *(long *) val1.value < (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x04) {
                FloatValue val1 = *(FloatValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x05) {
                DoubleValue val1 = *(DoubleValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value < (long double) val2.value;
                }
            }
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    else if (val1Raw->valueType == 0x01) {
        auto* val1 = (StringValue*) value1;
        if (val2Raw->valueType == 0x01) {
            auto* val2 = (StringValue*) value2;
            bool result = strcmp(val1->value, val2->value) < 0;
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    else if (val1Raw->valueType == 0x07) {
        auto* val1 = (BoolValue*) value1;
        if (val2Raw->valueType == 0x07) {
            auto* val2 = (BoolValue*) value2;
            bool result = val1->value < val2->value;
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
    resValue->value = false;
    resValue->base.valueType = 0x07;
    resValue->base.exp.base.type = const_cast<char *>("Value");
    resValue->base.exp.type = const_cast<char *>("Value");
    return resValue;
}

void* le(void* value1, void* value2) {
    auto* val1Raw = (Value*)value1;
    auto* val2Raw = (Value*)value2;
    if (val1Raw->valueType == 0x06 ||
        val1Raw->valueType == 0x02 ||
        val1Raw->valueType == 0x03 ||
        val1Raw->valueType == 0x04 ||
        val1Raw->valueType == 0x05) {
        if (val2Raw->valueType == 0x06 ||
            val2Raw->valueType == 0x02 ||
            val2Raw->valueType == 0x03 ||
            val2Raw->valueType == 0x04 ||
            val2Raw->valueType == 0x05) {
            bool result = false;
            if (val1Raw->valueType == 0x02) {
                ShortValue val1 = *(ShortValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x06) {
                IntValue val1 = *(IntValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x03) {
                LongValue val1 = *(LongValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) *(long *) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) *(long *) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) *(long *) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) *(long *) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) *(long *) val1.value <= (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x04) {
                FloatValue val1 = *(FloatValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x05) {
                DoubleValue val1 = *(DoubleValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value <= (long double) val2.value;
                }
            }
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    else if (val1Raw->valueType == 0x01) {
        auto* val1 = (StringValue*) value1;
        if (val2Raw->valueType == 0x01) {
            auto* val2 = (StringValue*) value2;
            bool result = strcmp(val1->value, val2->value) <= 0;
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    else if (val1Raw->valueType == 0x07) {
        auto* val1 = (BoolValue*) value1;
        if (val2Raw->valueType == 0x07) {
            auto* val2 = (BoolValue*) value2;
            bool result = val1->value <= val2->value;
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
    resValue->value = false;
    resValue->base.valueType = 0x07;
    resValue->base.exp.base.type = const_cast<char *>("Value");
    resValue->base.exp.type = const_cast<char *>("Value");
    return resValue;
}

void* ge(void* value1, void* value2) {
    auto* val1Raw = (Value*)value1;
    auto* val2Raw = (Value*)value2;
    if (val1Raw->valueType == 0x06 ||
        val1Raw->valueType == 0x02 ||
        val1Raw->valueType == 0x03 ||
        val1Raw->valueType == 0x04 ||
        val1Raw->valueType == 0x05) {
        if (val2Raw->valueType == 0x06 ||
            val2Raw->valueType == 0x02 ||
            val2Raw->valueType == 0x03 ||
            val2Raw->valueType == 0x04 ||
            val2Raw->valueType == 0x05) {
            bool result = false;
            if (val1Raw->valueType == 0x02) {
                ShortValue val1 = *(ShortValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x06) {
                IntValue val1 = *(IntValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x03) {
                LongValue val1 = *(LongValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) *(long *) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) *(long *) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) *(long *) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) *(long *) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) *(long *) val1.value >= (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x04) {
                FloatValue val1 = *(FloatValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x05) {
                DoubleValue val1 = *(DoubleValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value >= (long double) val2.value;
                }
            }
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    else if (val1Raw->valueType == 0x01) {
        auto* val1 = (StringValue*) value1;
        if (val2Raw->valueType == 0x01) {
            auto* val2 = (StringValue*) value2;
            bool result = strcmp(val1->value, val2->value) >= 0;
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    else if (val1Raw->valueType == 0x07) {
        auto* val1 = (BoolValue*) value1;
        if (val2Raw->valueType == 0x07) {
            auto* val2 = (BoolValue*) value2;
            bool result = val1->value >= val2->value;
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
    resValue->value = false;
    resValue->base.valueType = 0x07;
    resValue->base.exp.base.type = const_cast<char *>("Value");
    resValue->base.exp.type = const_cast<char *>("Value");
    return resValue;
}

void* gt(void* value1, void* value2) {
    auto* val1Raw = (Value*)value1;
    auto* val2Raw = (Value*)value2;
    if (val1Raw->valueType == 0x06 ||
        val1Raw->valueType == 0x02 ||
        val1Raw->valueType == 0x03 ||
        val1Raw->valueType == 0x04 ||
        val1Raw->valueType == 0x05) {
        if (val2Raw->valueType == 0x06 ||
            val2Raw->valueType == 0x02 ||
            val2Raw->valueType == 0x03 ||
            val2Raw->valueType == 0x04 ||
            val2Raw->valueType == 0x05) {
            bool result = false;
            if (val1Raw->valueType == 0x02) {
                ShortValue val1 = *(ShortValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x06) {
                IntValue val1 = *(IntValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x03) {
                LongValue val1 = *(LongValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) *(long *) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) *(long *) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) *(long *) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) *(long *) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) *(long *) val1.value > (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x04) {
                FloatValue val1 = *(FloatValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                }
            } else if (val1Raw->valueType == 0x05) {
                DoubleValue val1 = *(DoubleValue *) value1;
                if (val2Raw->valueType == 0x02) {
                    ShortValue val2 = *(ShortValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x06) {
                    IntValue val2 = *(IntValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x03) {
                    LongValue val2 = *(LongValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x04) {
                    FloatValue val2 = *(FloatValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                } else if (val2Raw->valueType == 0x05) {
                    DoubleValue val2 = *(DoubleValue *) value2;
                    result = (long double) val1.value > (long double) val2.value;
                }
            }
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    else if (val1Raw->valueType == 0x01) {
        auto* val1 = (StringValue*) value1;
        if (val2Raw->valueType == 0x01) {
            auto* val2 = (StringValue*) value2;
            bool result = strcmp(val1->value, val2->value) > 0;
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    else if (val1Raw->valueType == 0x07) {
        auto* val1 = (BoolValue*) value1;
        if (val2Raw->valueType == 0x07) {
            auto* val2 = (BoolValue*) value2;
            bool result = val1->value > val2->value;
            auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            resValue->value = result;
            resValue->base.valueType = 0x07;
            resValue->base.exp.base.type = const_cast<char *>("Value");
            resValue->base.exp.type = const_cast<char *>("Value");
            return resValue;
        }
    }
    auto *resValue = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
    resValue->value = false;
    resValue->base.valueType = 0x07;
    resValue->base.exp.base.type = const_cast<char *>("Value");
    resValue->base.exp.type = const_cast<char *>("Value");
    return resValue;
}

void* notOp(void* value) {
    auto* valueRaw = (Value*) value;
    if (valueRaw->valueType == 0x07) {
        auto* result = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
        result->value = !((BoolValue*)value)->value;
        result->base.exp.base.type = const_cast<char*>("Value");
        result->base.exp.type = const_cast<char*>("Value");
        result->base.valueType = 0x07;
        return result;
    }
    return nullptr;
}

void* resolveIndex(struct Index* index) {
    void* target = index->var;
    if (strcmp(((Code *) target)->type, "Array") == 0) {
        auto* array = (Array*)target;
        if (index->index->size == 1 && strcmp(((Code*)index->index->listPointer->data)->type, "Value") == 0) {
            auto* indexValue = (IntValue*)index->index->listPointer->data;
            return array->array[indexValue->value];
        }
        else if (index->index->size == 1 && strcmp(((Code*)index->index->listPointer->data)->type, "Period") == 0) {
            auto* indexValue = (struct Period*)index->index->listPointer->data;
            int start = ((IntValue*)indexValue->start)->value, end = ((IntValue*)indexValue->end)->value;
            auto* result = static_cast<Array *>(custom_alloc(sizeof(Array)));
            initArray(result, static_cast<size_t>(end - start));
            memcpy(result->array, array->array[start], static_cast<size_t>(end - start));
            return result;
        }
    }
    else if (strcmp(((Code*)target)->type, "Value") == 0) {
        auto* rawValue = (Value*)target;
        if (rawValue->valueType == 0x01) {
            auto* stringValue = (StringValue*)target;
            auto *result = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
            result->base.exp.base.type = const_cast<char *>("Value");
            result->base.exp.type = const_cast<char *>("Value");
            result->base.valueType = 0x01;
            if (index->index->size == 1 && strcmp(((Code*)index->index->listPointer->data)->type, "Value") == 0) {
                auto* indexValue = (IntValue*)index->index->listPointer->data;
                result->value = static_cast<char *>(custom_alloc(1));
                memcpy(result->value, &stringValue->value[indexValue->value], 1);
                return result;
            }
            else if (index->index->size == 1 && strcmp(((Code*)index->index->listPointer->data)->type, "Period") == 0) {
                auto* indexValue = (struct Period*)index->index->listPointer->data;
                int start = ((IntValue*)indexValue->start)->value, end = ((IntValue*)indexValue->end)->value;
                result->value = static_cast<char *>(custom_alloc(static_cast<size_t>(end - start)));
                memcpy(result->value, &(stringValue->value)[start], static_cast<size_t>(end - start));
                return result;
            }
        }
    }
    return nullptr;
}

void* resolveRef(void* refRaw) {
    void* objectChain = nullptr;
    void* target = nullptr;
    char* refStr = const_cast<char *>("");
    char* chainName;
    struct StackDataItem *iterator = dataStack.item;
    if (strcmp(((Code*)refRaw)->type, "Reference") == 0) {
        auto* ref = (struct Reference*) refRaw;
        chainName = static_cast<char *>(custom_alloc(strlen(ref->currentChain->id) + 1));
        strcpy(chainName, ref->currentChain->id);
        target = dict_get((Dictionary *) iterator->data, chainName);
        while (target == nullptr && iterator->prev != nullptr) {
            iterator = iterator->prev;
            target = dict_get((Dictionary *) iterator->data, ref->currentChain->id);
        }
        if (target != nullptr && strcmp(((Code *) target)->type, "Object") == 0)
            objectChain = target;
        refStr = chainName;
        refRaw = ref->restOfTheChain;
    }
    else if (strcmp(((Code*)refRaw)->type, "Identifier") == 0) {
        auto* ref = (Identifier*) refRaw;
        chainName = static_cast<char *>(custom_alloc(strlen(ref->id) + 1));
        strcpy(chainName, ref->id);
        target = dict_get((Dictionary *) iterator->data, chainName);
        while (target == nullptr && iterator->prev != nullptr) {
            iterator = iterator->prev;
            target = dict_get((Dictionary *) iterator->data, ref->id);
        }
        if (target != nullptr && strcmp(((Code *) target)->type, "Object") == 0)
            objectChain = target;
        refStr = chainName;
        refRaw = nullptr;
    }
    else if (strcmp(((Code*)refRaw)->type, "Index") == 0) {
        auto* index = (struct Index*) refRaw;
        Pair* tempResult = static_cast<Pair *>(resolveRef(index->var));
        refStr = static_cast<char *>(((Pair *) tempResult->second)->first);
        index->var = tempResult->first;
        target = resolveIndex(index);
        refRaw = index->restOfTheChain;
    }
    while (target != nullptr && refRaw != nullptr) {
        if (strcmp(((Code*)refRaw)->type, "Reference") == 0) {
            auto* ref = (struct Reference*) refRaw;
            if (strcmp(((Code*)target)->type, "Object") == 0) {
                chainName = static_cast<char *>(custom_alloc(strlen(ref->currentChain->id) + 1));
                strcpy(chainName, ref->currentChain->id);
                strcat(refStr, ".");
                strcat(refStr, chainName);
                void* target2 = dict_get(((Object*)target)->value, chainName);
                if (target2 != nullptr)
                    target = target2;
                else {
                    target2 = dict_get(((Object*)target)->funcs, chainName);
                    if (target2 != nullptr)
                        target = target2;
                }
                refRaw = ref->restOfTheChain;
            }
            else if (strcmp(((Code*)target)->type, "Class") == 0) {
                chainName = static_cast<char *>(custom_alloc(strlen(ref->currentChain->id) + 1));
                strcpy(chainName, ref->currentChain->id);
                strcat(refStr, ".");
                strcat(refStr, chainName);
                target = dict_get(((Class*)target)->functions, chainName);
                refRaw = ref->restOfTheChain;
            }
            else if (strcmp(((Code*)target)->type, "Function") == 0) {
                chainName = static_cast<char *>(custom_alloc(strlen(ref->currentChain->id) + 1));
                strcpy(chainName, ref->currentChain->id);
                strcat(refStr, ".");
                strcat(refStr, chainName);
                dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), target);
                dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value2"), refStr);
                dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value3"), objectChain);
                break;
            }
        }
        else if (strcmp(((Code*)refRaw)->type, "Index") == 0) {
            auto* index = (struct Index*) refRaw;
            Pair* tempResult = static_cast<Pair *>(resolveRef(index->var));
            refStr = static_cast<char *>(((Pair *) tempResult->second)->first);
            index->var = tempResult->first;
            target = resolveIndex(index);
            refRaw = index->restOfTheChain;
        }
    }
    Pair* result = static_cast<Pair *>(custom_alloc(sizeof(Pair)));
    result->first = target;
    Pair* result2 = static_cast<Pair *>(custom_alloc(sizeof(Pair)));
    result2->first = refStr;
    result2->second = objectChain;
    result->second = result2;
    return result;
}

int convertBytesToInt(const char bytes[]) {
    return (bytes[3] & 0xff) | ((bytes[2] & 0xff) << 8) | ((bytes[1] & 0xff) << 16) | ((bytes[0] & 0xff) << 24);
}

short convertBytesToShort(const char bytes[]) {
    return static_cast<short>((bytes[1] & 0xff) | ((bytes[0] & 0xff) << 8));
}

unsigned long calculateBytes(int investigateId, char* c, unsigned long p) {
    while (!expStack.isEmpty(&expStack)) {
        if (c[p] == 0x4d) {
            p++;
            auto* empty = static_cast<Empty *>(custom_alloc(sizeof(Empty)));
            dict_add(static_cast<Dictionary *>(expStack.top(&expStack)), const_cast<char *>("value"), empty);
            return p;
        }
        if (c[p] == 0x4e) {
            p++;
            char countBytes[4];
            for (int i = 0; i < (int)sizeof(countBytes); i++)
                countBytes[i] = c[p + i];
            p += (int) sizeof(countBytes);
            int count = convertBytesToInt(countBytes);
            auto* obj = static_cast<Object *>(custom_alloc(sizeof(Object)));
            obj->base.type = const_cast<char *>("Object");
            obj->funcs = dict_new();
            obj->value = dict_new();
            for (int counter = 0; counter < count; counter++) {
                if (c[p] == 0x01) {
                    p++;
                    char keyLengthBytes[4];
                    for (int i = 0; i < (int)sizeof(keyLengthBytes); i++)
                        keyLengthBytes[i] = c[p + i];
                    p += (int) sizeof(keyLengthBytes);
                    int keyLength = convertBytesToInt(keyLengthBytes);
                    char keyBytes[keyLength];
                    for (int i = 0; i < keyLength; i++)
                        keyBytes[i] = c[p + i];
                    p += keyLength;
                    char* key = static_cast<char *>(custom_alloc(strlen(keyBytes) + 1));
                    strcpy(key, keyBytes);
                    expStack.push(&expStack, dict_new());
                    p = calculateBytes(investigateId, c, p);
                    void* exp = dict_get(static_cast<Dictionary *>(expStack.pop(&expStack)),
                                         const_cast<char *>("value"));
                    dict_add(obj->value, key, exp);
                }
            }
            dict_add(static_cast<Dictionary *>(expStack.top(&expStack)), const_cast<char *>("value"), obj);
            return p;
        }
        else if (c[p] == 0x4f) {
            p++;
            if (c[p] == 0x01) {
                p++;
                expStack.push(&expStack, dict_new());
                p = calculateBytes(investigateId, c, p);
                dict_add(static_cast<Dictionary *>(expStack.top(&expStack)), const_cast<char *>("value"),
                         reinterpret_cast<void *>(notOp(dict_get(
                                 static_cast<Dictionary *>(expStack.pop(&expStack)), const_cast<char *>("value")))));
                return p;
            }
        }
        else if (c[p] == 0x68) {
            p++;
            auto* array = static_cast<Array *>(custom_alloc(sizeof(Array)));
            array->base.type = const_cast<char *>("Array");
            if (c[p] == 0x01) {
                p++;
                char itemsCountBytes[4];
                for (int i = 0; i < (int) sizeof(itemsCountBytes); i++)
                    itemsCountBytes[i] = c[p + i];
                p += (int) sizeof(itemsCountBytes);
                int itemsCount = convertBytesToInt(itemsCountBytes);
                initArray(array, static_cast<size_t>(itemsCount));
                for (int i = 0; i < itemsCount; i++) {
                    if (c[p] == 0x02) {
                        p++;
                        expStack.push(&expStack, dict_new());
                        p = calculateBytes(investigateId, c, p);
                        void* value = dict_get((Dictionary*)expStack.pop(&expStack), const_cast<char *>("value"));
                        insertArray(array, value);
                    }
                }
                dict_add((Dictionary*)expStack.top(&expStack), const_cast<char *>("value"), array);
                return p;
            }
        }
        else if (c[p] == 0x7f) {
            p++;
            auto *ref = static_cast<Reference *>(custom_alloc(sizeof(struct Reference)));
            ref->base.type = const_cast<char *>("Reference");
            if (c[p] == 0x01) {
                p++;
                expStack.push(&expStack, dict_new());
                p = calculateBytes(0, c, p);
                auto *id = (Identifier *) dict_get((Dictionary *) expStack.pop(&expStack),
                                                         const_cast<char *>("value"));
                ref->currentChain = id;
                if (c[p] == 0x02) {
                    p++;
                    expStack.push(&expStack, dict_new());
                    p = calculateBytes(0, c, p);
                    ref->restOfTheChain = dict_get((Dictionary *) expStack.pop(&expStack), const_cast<char *>("value"));
                }
                if (c[p] == 0x6d) {
                    p++;
                    if (investigateId == 1) {
                        Pair* result = static_cast<Pair *>(resolveRef(ref));
                        dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), result->first);
                        dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value2"), ((Pair*)result->second)->first);
                        dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value3"), ((Pair*)result->second)->second);
                    } else {
                        dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), ref);
                    }
                    return p;
                }
            }
        }
        else if (c[p] == 0x6c) {
            p++;
            auto* index = static_cast<Index *>(custom_alloc(sizeof(struct Index)));
            index->base.type = const_cast<char *>("Index");
            if (c[p] == 0x01) {
                p++;
                expStack.push(&expStack, dict_new());
                p = calculateBytes(0, c, p);
                void* var = dict_get((Dictionary*)expStack.pop(&expStack), const_cast<char *>("value"));
                index->var = var;
                if (c[p] == 0x02) {
                    p++;
                    char indicesCountBytes[4];
                    for (int i = 0; i < (int) sizeof(indicesCountBytes); i++)
                        indicesCountBytes[i] = c[p + i];
                    p += (int) sizeof(indicesCountBytes);
                    int indicesCount = convertBytesToInt(indicesCountBytes);
                    auto* indicesList = static_cast<List *>(custom_alloc(sizeof(struct List)));
                    initList(indicesList);
                    for (int i = 0; i < indicesCount; i++) {
                        if (c[p] == 0x03) {
                            p++;
                            expStack.push(&expStack, dict_new());
                            p = calculateBytes(investigateId, c, p);
                            void *indexItem = dict_get((Dictionary *) expStack.pop(&expStack),
                                                       const_cast<char *>("value"));
                            indicesList->append(indicesList, indexItem);
                        }
                    }
                    index->index = indicesList;
                    if (c[p] == 0x04) {
                        p++;
                        expStack.push(&expStack, dict_new());
                        p = calculateBytes(investigateId, c, p);
                        void* restOfChains = dict_get((Dictionary *) expStack.pop(&expStack),
                                                      const_cast<char *>("value"));
                        index->restOfTheChain = restOfChains;
                    }
                    if (c[p] == 0x6b) {
                        p++;
                        if (investigateId == 1) {
                            Pair* result = static_cast<Pair *>(resolveRef(index));
                            dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), result->first);
                            dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value2"), ((Pair*)result->second)->first);
                            dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value3"), ((Pair*)result->second)->second);
                        } else {
                            dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), index);
                        }
                        return p;
                    }
                }
            }
        }
        else if (c[p] == 0x6a) {
            p++;
            auto* period = static_cast<Period *>(custom_alloc(sizeof(struct Period)));
            period->base.type = const_cast<char *>("Period");
            if (c[p] == 0x01) {
                p++;
                expStack.push(&expStack, dict_new());
                p = calculateBytes(investigateId, c, p);
                period->start = dict_get((Dictionary*)expStack.pop(&expStack), const_cast<char *>("value"));
                if (c[p] == 0x02) {
                    p++;
                    expStack.push(&expStack, dict_new());
                    p = calculateBytes(investigateId, c, p);
                    period->end = dict_get((Dictionary *) expStack.pop(&expStack), const_cast<char *>("value"));
                    if (c[p] == 0x69) {
                        p++;
                        dict_add((Dictionary*)expStack.top(&expStack), const_cast<char *>("value"), period);
                        return p;
                    }
                }
            }
        }
        else if (c[p] == 0x55) {
            p++;
            if (c[p] == 0x01) {
                p++;
                Dictionary* expDict = dict_new();
                expStack.push(&expStack, expDict);
                p = calculateBytes(1, c, p);
                void* target = dict_get((Dictionary*)expStack.top(&expStack), const_cast<char *>("value"));
                char* refStr = static_cast<char *>(dict_get((Dictionary *) expStack.top(&expStack),
                                                            const_cast<char *>("value2")));
                auto* thisObj = static_cast<Object *>(dict_get((Dictionary *) expStack.pop(&expStack),
                                                                 const_cast<char *>("value3")));
                if (c[p] == 0x02) {
                    p++;
                    char entriesCountBytes[4];
                    for (int index = 0; index < 4; index++)
                        entriesCountBytes[index] = c[p + index];
                    p += 4;
                    int entriesCount = convertBytesToInt(entriesCountBytes);
                    Dictionary* entriesDict = dict_new();
                    for (int counter = 0; counter < entriesCount; counter++) {
                        if (c[p] == 0x03) {
                            p++;
                            char keyLengthBytes[4];
                            for (int index = 0; index < 4; index++)
                                keyLengthBytes[index] = c[p + index];
                            p += 4;
                            int keyLength = convertBytesToInt(keyLengthBytes);
                            char keyBytes[keyLength];
                            for (int index = 0; index < keyLength; index++)
                                keyBytes[index] = c[p + index];
                            p += keyLength;
                            char *key = static_cast<char *>(custom_alloc(strlen(keyBytes) + 1));
                            strcpy(key, keyBytes);
                            char valueLengthBytes[4];
                            for (int index = 0; index < 4; index++)
                                valueLengthBytes[index] = c[p + index];
                            p += 4;
                            Dictionary *expD = dict_new();
                            expStack.push(&expStack, expD);
                            p = calculateBytes(1, c, p);
                            dict_add(entriesDict, key, dict_get((Dictionary *) expStack.pop(&expStack),
                                                                const_cast<char *>("value")));
                        }
                    }
                    if (target == nullptr) {
                        dict_add((Dictionary*)expStack.top(&expStack), const_cast<char *>("value"), routeAndResolve(refStr, entriesDict));
                    } else {
                        auto* func = (Function*) target;
                        if (thisObj != nullptr)
                            dict_add(entriesDict, const_cast<char *>("this"), thisObj);
                        dict_add((Dictionary*)expStack.top(&expStack), const_cast<char *>("value"), executeIntern(func->codes, func->loc, entriesDict));
                    }
                    return p;
                }
            }
        } else if (c[p] == 0x57) {
            p++;
            if (c[p] == 0x01) {
                p++;
                Dictionary* expDict = dict_new();
                expStack.push(&expStack, expDict);
                p = calculateBytes(1, c, p);
                void* target = dict_get((Dictionary*)expStack.pop(&expStack), const_cast<char *>("value"));
                if (c[p] == 0x02) {
                    p++;
                    char entriesCountBytes[4];
                    for (int index = 0; index < 4; index++)
                        entriesCountBytes[index] = c[p + index];
                    p += 4;
                    int entriesCount = convertBytesToInt(entriesCountBytes);
                    Dictionary* entriesDict = dict_new();
                    for (int counter = 0; counter < entriesCount; counter++) {
                        if (c[p] == 0x03) {
                            p++;
                            char keyLengthBytes[4];
                            for (int index = 0; index < 4; index++)
                                keyLengthBytes[index] = c[p + index];
                            p += 4;
                            int keyLength = convertBytesToInt(keyLengthBytes);
                            char keyBytes[keyLength];
                            for (int index = 0; index < keyLength; index++)
                                keyBytes[index] = c[p + index];
                            p += keyLength;
                            char *key = static_cast<char *>(custom_alloc(strlen(keyBytes) + 1));
                            strcpy(key, keyBytes);
                            char valueLengthBytes[4];
                            for (int index = 0; index < 4; index++)
                                valueLengthBytes[index] = c[p + index];
                            p += 4;
                            expStack.push(&expStack, dict_new());
                            p = calculateBytes(1, c, p);
                            void* data = dict_get((Dictionary *) expStack.pop(&expStack), const_cast<char *>("value"));
                            dict_add(entriesDict, key, data);
                        }
                    }
                    if (target != nullptr) {
                        auto* classObj = (Class*) target;
                        auto* object = static_cast<Object *>(custom_alloc(sizeof(Object)));
                        object->base.type = const_cast<char *>("Object");
                        object->value = dict_new();
                        while (classObj->properties->iteratorHasNext(classObj->properties)) {
                            Prop* prop = (Prop*) classObj->properties->iteratorForward(classObj->properties);
                            char* propId = static_cast<char *>(custom_alloc(strlen(prop->id->id) + 1));
                            strcpy(propId, prop->id->id);
                            expStack.push(&expStack, dict_new());
                            calculateBytes(1, prop->value, 0);
                            dict_add(object->value, propId, dict_get((Dictionary*)expStack.pop(&expStack),
                                                                     const_cast<char *>("value")));
                        }
                        struct ListDataItem* iterator = classObj->constructor->params->listPointer;
                        while (iterator != nullptr) {
                            if (dict_get(entriesDict, ((Identifier*)iterator->data)->id) == nullptr) {
                                auto* empty = static_cast<Empty *>(custom_alloc(sizeof(Empty)));
                                empty->base.type = const_cast<char *>("Empty");
                                dict_add(entriesDict, ((Identifier*)iterator->data)->id, empty);
                            }
                            iterator = iterator->prev;
                        }
                        object->funcs = classObj->functions;
                        if (classObj->constructor != nullptr) {
                            dict_add(entriesDict, const_cast<char *>("this"), object);
                            executeIntern(classObj->constructor->body, classObj->constructor->loc, entriesDict);
                        }
                        dict_add((Dictionary*)expStack.top(&expStack), const_cast<char *>("value"), object);
                    }
                    return p;
                }
            }
        } else if (c[p] == 0x71) {
            p++;
            if (c[p] == 0x01) {
                p++;
                expStack.push(&expStack, dict_new());
                p = calculateBytes(1, c, p);
                void* value1 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                if (c[p] == 0x02) {
                    p++;
                    expStack.push(&expStack, dict_new());
                    p = calculateBytes(1, c, p);
                    void* value2 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                    void* result = sum(value1, value2);
                    dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), result);
                    return p;
                }
            }
        } else if (c[p] == 0x72) {
            p++;
            if (c[p] == 0x01) {
                p++;
                Dictionary* dict = dict_new();
                expStack.push(&expStack, dict);
                p = calculateBytes(1, c, p);
                void* value1 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                if (c[p] == 0x02) {
                    p++;
                    expStack.push(&expStack, dict_new());
                    p = calculateBytes(1, c, p);
                    void* value2 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                    void* result = subtract(value1, value2);
                    dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), result);
                    return p;
                }
            }
        } else if (c[p] == 0x73) {
            p++;
            if (c[p] == 0x01) {
                p++;
                Dictionary* dict = dict_new();

                expStack.push(&expStack, dict);
                p = calculateBytes(1, c, p);
                void* value1 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                if (c[p] == 0x02) {
                    p++;
                    Dictionary* dict2 = dict_new();
                    expStack.push(&expStack, dict2);
                    p = calculateBytes(1, c, p);
                    void* value2 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                    void* result = multiply(value1, value2);
                    dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), result);
                    return p;
                }
            }
        } else if (c[p] == 0x74) {
            p++;
            if (c[p] == 0x01) {
                p++;
                Dictionary* dict = dict_new();
                expStack.push(&expStack, dict);
                p = calculateBytes(1, c, p);
                void* value1 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                if (c[p] == 0x02) {
                    p++;
                    Dictionary* dict2 = dict_new();
                    expStack.push(&expStack, dict2);
                    p = calculateBytes(1, c, p);
                    void* value2 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                    void* result = divide(value1, value2);
                    dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), result);
                    return p;
                }
            }
        } else if (c[p] == 0x75) {
            p++;
            if (c[p] == 0x01) {
                p++;
                Dictionary* dict = dict_new();
                expStack.push(&expStack, dict);
                p = calculateBytes(1, c, p);
                void* value1 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                if (c[p] == 0x02) {
                    p++;
                    Dictionary* dict2 = dict_new();
                    expStack.push(&expStack, dict2);
                    p = calculateBytes(1, c, p);
                    void* value2 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                    void* result = mod(value1, value2);
                    dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), result);
                    return p;
                }
            }
        } else if (c[p] == 0x76) {
            p++;
            if (c[p] == 0x01) {
                p++;
                Dictionary* dict = dict_new();
                expStack.push(&expStack, dict);
                p = calculateBytes(1, c, p);
                void* value1 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                if (c[p] == 0x02) {
                    p++;
                    Dictionary* dict2 = dict_new();
                    expStack.push(&expStack, dict2);
                    p = calculateBytes(1, c, p);
                    void* value2 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                    void* result = power(value1, value2);
                    dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), result);
                    return p;
                }
            }
        } else if (c[p] == 0x77) {
            p++;
            if (c[p] == 0x01) {
                p++;
                Dictionary* dict = dict_new();
                expStack.push(&expStack, dict);
                p = calculateBytes(1, c, p);
                void* value1 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                if (c[p] == 0x02) {
                    p++;
                    Dictionary* dict2 = dict_new();
                    expStack.push(&expStack, dict2);
                    p = calculateBytes(1, c, p);
                    void* value2 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                    void* result = andOp(value1, value2);
                    dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), result);
                    return p;
                }
            }
        } else if (c[p] == 0x78) {
            p++;
            if (c[p] == 0x01) {
                p++;
                expStack.push(&expStack, dict_new());
                p = calculateBytes(1, c, p);
                void* value1 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                if (c[p] == 0x02) {
                    p++;
                    expStack.push(&expStack, dict_new());
                    p = calculateBytes(1, c, p);
                    void* value2 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                    void* result = orOp(value1, value2);
                    dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), result);
                    return p;
                }
            }
        } else if (c[p] == 0x79) {
            p++;
            if (c[p] == 0x01) {
                p++;
                Dictionary* dict = dict_new();

                expStack.push(&expStack, dict);
                p = calculateBytes(1, c, p);
                void* value1 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                if (c[p] == 0x02) {
                    p++;
                    Dictionary* dict2 = dict_new();

                    expStack.push(&expStack, dict2);
                    p = calculateBytes(1, c, p);
                    void* value2 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                    void* result = equal(value1, value2);
                    dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), result);
                    return p;
                }
            }
        } else if (c[p] == 0x7a) {
            p++;
            if (c[p] == 0x01) {
                p++;
                Dictionary* dict = dict_new();

                expStack.push(&expStack, dict);
                p = calculateBytes(1, c, p);
                void* value1 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                if (c[p] == 0x02) {
                    p++;
                    Dictionary* dict2 = dict_new();
                    expStack.push(&expStack, dict2);
                    p = calculateBytes(1, c, p);
                    void* value2 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                    void* result = gt(value1, value2);
                    dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), result);
                    return p;
                }
            }
        } else if (c[p] == 0x7b) {
            p++;
            if (c[p] == 0x01) {
                p++;
                Dictionary* dict = dict_new();

                expStack.push(&expStack, dict);
                p = calculateBytes(1, c, p);
                void* value1 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                if (c[p] == 0x02) {
                    p++;
                    Dictionary* dict2 = dict_new();

                    expStack.push(&expStack, dict2);
                    p = calculateBytes(1, c, p);
                    void* value2 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                    void* result = ge(value1, value2);
                    dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), result);
                    return p;
                }
            }
        } else if (c[p] == 0x7c) {
            p++;
            if (c[p] == 0x01) {
                p++;
                Dictionary* dict = dict_new();

                expStack.push(&expStack, dict);
                p = calculateBytes(1, c, p);
                void* value1 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                if (c[p] == 0x02) {
                    p++;
                    Dictionary* dict2 = dict_new();

                    expStack.push(&expStack, dict2);
                    p = calculateBytes(1, c, p);
                    void* value2 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                    void* result = ne(value1, value2);
                    dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), result);
                    return p;
                }
            }
        } else if (c[p] == 0x7d) {
            p++;
            if (c[p] == 0x01) {
                p++;
                Dictionary* dict = dict_new();

                expStack.push(&expStack, dict);
                p = calculateBytes(1, c, p);
                void* value1 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                if (c[p] == 0x02) {
                    p++;
                    Dictionary* dict2 = dict_new();
                    expStack.push(&expStack, dict2);
                    p = calculateBytes(1, c, p);
                    void* value2 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                    void* result = le(value1, value2);
                    dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), result);
                    return p;
                }
            }
        } else if (c[p] == 0x7e) {
            p++;
            if (c[p] == 0x01) {
                p++;
                Dictionary* dict = dict_new();

                expStack.push(&expStack, dict);
                p = calculateBytes(1, c, p);
                void* value1 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                if (c[p] == 0x02) {
                    p++;
                    Dictionary* dict2 = dict_new();

                    expStack.push(&expStack, dict2);
                    p = calculateBytes(1, c, p);
                    void* value2 = dict_get((Dictionary*) expStack.pop(&expStack), const_cast<char *>("value"));
                    void* result = lt(value1, value2);
                    dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), result);
                    return p;
                }
            }
        }
        else if (c[p] == 0x61) {
            p++;
            char idNameLengthArr[4];
            for (int index = 0; index < (int)sizeof(idNameLengthArr); index++)
                idNameLengthArr[index] = c[p + index];
            p += 4;
            int idNameLength = convertBytesToInt(idNameLengthArr);
            char idNameArr[idNameLength];
            for (int index = 0; index < idNameLength; index++)
                idNameArr[index] = c[p + index];
            p += idNameLength;
            auto* id = static_cast<Identifier *>(custom_alloc(sizeof(Identifier)));
            unsigned long size = strlen(idNameArr) + 1;
            id->id = static_cast<char *>(custom_alloc(size));
            strcpy(id->id, idNameArr);
            id->exp.type = const_cast<char *>("Identifier");
            id->exp.base.type = const_cast<char *>("Identifier");
            if (investigateId == 1) {
                struct StackDataItem *iterator = dataStack.item;
                char* idName = static_cast<char *>(custom_alloc(strlen(id->id) + 1));
                strcpy(idName, id->id);
                void *value = dict_get((Dictionary *) iterator->data, idName);
                while (value == nullptr && iterator->prev != nullptr) {
                    iterator = iterator->prev;
                    value = dict_get((Dictionary *) iterator->data, idName);
                }
                if (value == nullptr)
                    dict_add((Dictionary *) expStack.top(&expStack), const_cast<char *>("value"), id);
                else
                    dict_add((Dictionary *) expStack.top(&expStack), const_cast<char *>("value"), value);
            }
            else {
                dict_add((Dictionary *) expStack.top(&expStack), const_cast<char *>("value"), id);
            }
            return p;
        } else if (c[p] == 0x62) {
            p++;
            char valueLengthArr[4];
            for (int index = 0; index < (int)sizeof(valueLengthArr); index++)
                valueLengthArr[index] = c[p + index];
            p += (int)sizeof(valueLengthArr);
            int valueLength = convertBytesToInt(valueLengthArr);
            char valueArr[valueLength];
            for (int index = 0; index < valueLength; index++)
                valueArr[index] = c[p + index];
            p += valueLength;
            auto* val = static_cast<StringValue *>(custom_alloc(sizeof(StringValue)));
            val->value = static_cast<char *>(custom_alloc(strlen(valueArr) + 1));
            strcpy(val->value, valueArr);
            val->base.valueType = 0x01;
            val->base.exp.type = const_cast<char *>("Value");
            val->base.exp.base.type = const_cast<char *>("Value");
            dict_add((Dictionary*)expStack.top(&expStack), const_cast<char *>("value"), val);
            return p;
        } else if (c[p] == 0x63) {
            p++;
            char valueArr[8];
            for (int index = 0; index < (int)sizeof(valueArr); index++)
                valueArr[index] = c[p + index];
            p += (int)sizeof(valueArr);
            char* valueRaw = valueArr;
            double value;
            memcpy(&value, valueRaw, sizeof(double));
            auto* val = static_cast<DoubleValue *>(custom_alloc(sizeof(DoubleValue)));
            val->value = value;
            val->base.valueType = 0x05;
            val->base.exp.type = const_cast<char *>("Value");
            val->base.exp.base.type = const_cast<char *>("Value");
            dict_add((Dictionary*)expStack.top(&expStack), const_cast<char *>("value"), val);
            return p;
        } else if (c[p] == 0x64) {
            p++;
            char valueArr[4];
            for (int index = 0; index < (int)sizeof(valueArr); index++)
                valueArr[index] = c[p + index];
            p += (int)sizeof(valueArr);
            char* valueRaw = valueArr;
            float value;
            memcpy(&value, valueRaw, sizeof(float));
            auto* val = static_cast<FloatValue *>(custom_alloc(sizeof(FloatValue)));
            val->value = value;
            val->base.valueType = 0x04;
            val->base.exp.type = const_cast<char *>("Value");
            val->base.exp.base.type = const_cast<char *>("Value");
            dict_add((Dictionary*)expStack.top(&expStack), const_cast<char *>("value"), val);
            return p;
        } else if (c[p] == 0x65) {
            p++;
            char valueArr[2];
            for (int index = 0; index < (int)sizeof(valueArr); index++)
                valueArr[index] = c[p + index];
            p += (int)sizeof(valueArr);
            auto* val = static_cast<ShortValue *>(custom_alloc(sizeof(ShortValue)));
            val->value = convertBytesToShort(valueArr);
            val->base.valueType = 0x02;
            val->base.exp.type = const_cast<char *>("Value");
            val->base.exp.base.type = const_cast<char *>("Value");
            dict_add((Dictionary*)expStack.top(&expStack), const_cast<char *>("value"), val);
            return p;
        } else if (c[p] == 0x66) {
            p++;
            char valueArr[4];
            for (int index = 0; index < (int)sizeof(valueArr); index++)
                valueArr[index] = c[p + index];
            p += (int)sizeof(valueArr);
            char* valueRaw = valueArr;
            int value;
            memcpy(&value, valueRaw, sizeof(int));
            auto* val = static_cast<IntValue *>(custom_alloc(sizeof(IntValue)));
            val->value = value;
            val->base.valueType = 0x06;
            val->base.exp.type = const_cast<char *>("Value");
            val->base.exp.base.type = const_cast<char *>("Value");
            dict_add((Dictionary*)expStack.top(&expStack), const_cast<char *>("value"), val);
            return p;
        } else if (c[p] == 0x67) {
            p++;
            char valueArr[8];
            for (int index = 0; index < (int)sizeof(valueArr); index++)
                valueArr[index] = c[p + index];
            p += (int)sizeof(valueArr);
            char* valueRaw = valueArr;
            long value;
            memcpy(&value, valueRaw, sizeof(long));
            auto* val = static_cast<LongValue *>(custom_alloc(sizeof(LongValue)));
            val->value = value;
            val->base.valueType = 0x03;
            val->base.exp.type = const_cast<char *>("Value");
            val->base.exp.base.type = const_cast<char *>("Value");
            dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), val);
            return p;
        } else if (c[p] == 0x68) {
            p++;
            char value = c[p];
            p++;
            bool finalValue = false;
            if ((int)value == 1) finalValue = true;
            auto* val = static_cast<BoolValue *>(custom_alloc(sizeof(BoolValue)));
            val->value = finalValue;
            val->base.valueType = 0x07;
            val->base.exp.type = const_cast<char *>("Value");
            val->base.exp.base.type = const_cast<char *>("Value");
            dict_add((Dictionary*) expStack.top(&expStack), const_cast<char *>("value"), val);
            return p;
        }
    }
    return 0;
}

void calculate(int investigateId) {
    pointer = calculateBytes(investigateId, code, pointer);
}

void* ride() {
    while (codeLength > pointer) {
        if (code[pointer] == 0x6e) {
            break;
        }
        if (code[pointer] == 0x4c) {
            return (void *) "break";
        }
        else if (code[pointer] == 0x51) {
            pointer++;
            auto* function = static_cast<Function *>(custom_alloc(sizeof(Function)));
            function->base.type = const_cast<char *>("Function");
            if (code[pointer] == 0x01) {
                pointer++;
                char funcNameLengthBytes[4];
                for (int index = 0; index < (int)sizeof(funcNameLengthBytes); index++)
                    funcNameLengthBytes[index] = code[pointer + index];
                pointer += (int)sizeof(funcNameLengthBytes);
                int funcNameLength = convertBytesToInt(funcNameLengthBytes);
                char funcName[funcNameLength];
                for (int index = 0; index < funcNameLength; index++)
                    funcName[index] = code[pointer + index];
                pointer += funcNameLength;
                char* funcNameStr = static_cast<char *>(custom_alloc(sizeof(funcName)));
                strcpy(funcNameStr, funcName);
                if (code[pointer] == 0x02) {
                    pointer++;
                    char funcLevelLengthBytes[4];
                    for (int index = 0; index < (int)sizeof(funcLevelLengthBytes); index++)
                        funcLevelLengthBytes[index] = code[pointer + index];
                    pointer += (int)sizeof(funcLevelLengthBytes);
                    int funcLevelLength = convertBytesToInt(funcLevelLengthBytes);
                    char funcLevelStr[funcLevelLength];
                    for (int index = 0; index < funcLevelLength; index++)
                        funcLevelStr[index] = code[pointer + index];
                    pointer += funcLevelLength;
                    if (code[pointer] == 0x03) {
                        pointer++;
                        auto* identifiers = static_cast<List *>(custom_alloc(sizeof(struct List)));
                        initList(identifiers);
                        char paramsCountBytes[4];
                        for (int index = 0; index < 4; index++)
                            paramsCountBytes[index] = code[pointer + index];
                        pointer += 4;
                        int paramsCount = convertBytesToInt(paramsCountBytes);
                        for (int counter = 0; counter < paramsCount; counter++) {
                            char paramLengthBytes[4];
                            for (int index = 0; index < (int)sizeof(paramLengthBytes); index++)
                                paramLengthBytes[index] = code[pointer + index];
                            pointer += (int)sizeof(paramLengthBytes);
                            int paramLength = convertBytesToInt(paramLengthBytes);
                            char idStr[paramLength];
                            for (int index = 0; index < paramLength; index++)
                                idStr[index] = code[pointer + index];
                            pointer += paramLength;
                            char* id = static_cast<char *>(custom_alloc(sizeof(idStr)));
                            strcpy(id, idStr);
                            identifiers->append(identifiers, id);
                        }
                        if (code[pointer] == 0x04) {
                            pointer++;
                            if (code[pointer] == 0x6f) {
                                pointer++;
                                char jumpBytes[4];
                                for (int index = 0; index < (int)sizeof(jumpBytes); index++)
                                    jumpBytes[index] = code[pointer + index];
                                pointer += (int)sizeof(jumpBytes);
                                int jump = convertBytesToInt(jumpBytes);
                                char body[jump];
                                for(unsigned long i = pointer; i < pointer + jump; i++)
                                    body[i - pointer] = code[i];
                                pointer += jump;
                                if (code[pointer] == 0x6e) {
                                    pointer++;
                                    function->base.type = const_cast<char *>("Function");
                                    function->funcName = funcNameStr;
                                    function->params = identifiers;
                                    function->codes = static_cast<char *>(custom_alloc(static_cast<size_t>(jump)));
                                    memcpy(function->codes, body, static_cast<size_t>(jump));
                                    function->loc = static_cast<unsigned long>(jump);
                                    dict_add((Dictionary *) dataStack.top(&dataStack), funcNameStr, function);
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (code[pointer] == 0x57) {
            pointer++;
            if (code[pointer] == 0x01) {
                pointer++;
                Dictionary* expDict = dict_new();
                expStack.push(&expStack, expDict);
                calculate(1);
                void* target = dict_get((Dictionary*)expStack.pop(&expStack), const_cast<char *>("value"));
                if (code[pointer] == 0x02) {
                    pointer++;
                    char entriesCountBytes[4];
                    for (int index = 0; index < 4; index++)
                        entriesCountBytes[index] = code[pointer + index];
                    pointer += 4;
                    int entriesCount = convertBytesToInt(entriesCountBytes);
                    Dictionary* entriesDict = dict_new();
                    for (int counter = 0; counter < entriesCount; counter++) {
                        if (code[pointer] == 0x03) {
                            pointer++;
                            char keyLengthBytes[4];
                            for (int index = 0; index < 4; index++)
                                keyLengthBytes[index] = code[pointer + index];
                            pointer += 4;
                            int keyLength = convertBytesToInt(keyLengthBytes);
                            char keyBytes[keyLength];
                            for (int index = 0; index < keyLength; index++)
                                keyBytes[index] = code[pointer + index];
                            pointer += keyLength;
                            char *key = &keyBytes[0];
                            char valueLengthBytes[4];
                            for (int index = 0; index < 4; index++)
                                valueLengthBytes[index] = code[pointer + index];
                            pointer += 4;
                            Dictionary *expD = dict_new();
                            expStack.push(&expStack, expD);
                            calculate(1);
                            dict_add(entriesDict, key, dict_get((Dictionary *) expStack.pop(&expStack),
                                                                const_cast<char *>("value")));
                        }
                    }
                    if (target != nullptr) {
                        auto* classObj = (Class*) target;
                        auto *object = static_cast<Object *>(custom_alloc(sizeof(Object)));
                        object->base.type = const_cast<char *>("Object");
                        object->value = dict_new();
                        while (classObj->properties->iteratorHasNext(classObj->properties)) {
                            Prop *prop = (Prop *) classObj->properties->iteratorForward(classObj->properties);
                            char *propId = static_cast<char *>(custom_alloc(strlen(prop->id->id) + 1));
                            strcpy(propId, prop->id->id);
                            expStack.push(&expStack, dict_new());
                            calculateBytes(1, prop->value, 0);
                            dict_add(object->value, propId, dict_get((Dictionary *) expStack.pop(&expStack),
                                                                     const_cast<char *>("value")));
                        }
                        struct ListDataItem* iterator = classObj->constructor->params->listPointer;
                        while (iterator != nullptr) {
                            if (dict_get(entriesDict, ((Identifier*)iterator->data)->id) == nullptr) {
                                auto* empty = static_cast<Empty *>(custom_alloc(sizeof(Empty)));
                                dict_add(entriesDict, ((Identifier*)iterator->data)->id, empty);
                            }
                            iterator = iterator->prev;
                        }
                        object->funcs = classObj->functions;
                        if (classObj->constructor != nullptr) {
                            dict_add(entriesDict, const_cast<char *>("this"), object);
                            executeIntern(classObj->constructor->body, classObj->constructor->loc, entriesDict);
                        }
                    }
                }
            }
        }
        else if (code[pointer] == 0x58) {
            pointer++;
            auto* classObj = static_cast<Class *>(custom_alloc(sizeof(Class)));
            classObj->base.type = const_cast<char *>("Class");
            if (code[pointer] == 0x01) {
                pointer++;
                char classNameLengthBytes[4];
                for (int index = 0; index < (int)sizeof(classNameLengthBytes); index++)
                    classNameLengthBytes[index] = code[pointer + index];
                pointer += (int)sizeof(classNameLengthBytes);
                int classNameLength = convertBytesToInt(classNameLengthBytes);
                char className[classNameLength];
                for (int index = 0; index < classNameLength; index++)
                    className[index] = code[pointer + index];
                pointer += classNameLength;
                char* classNameStr = static_cast<char *>(custom_alloc(strlen(className) + 1));
                strcpy(classNameStr, className);
                if (code[pointer] == 0x02) {
                    pointer++;
                    char inheritanceCountBytes[4];
                    for (int index = 0; index < (int)sizeof(inheritanceCountBytes); index++)
                        inheritanceCountBytes[index] = code[pointer + index];
                    pointer += (int)sizeof(inheritanceCountBytes);
                    int inheritanceCount = convertBytesToInt(inheritanceCountBytes);
                    auto* inheritance = static_cast<List *>(custom_alloc(sizeof(struct List)));
                    initList(inheritance);
                    for (int i = 0; i < inheritanceCount; i++) {
                        char identifierNameLengthBytes[4];
                        for (int index = 0; index < (int)sizeof(identifierNameLengthBytes); index++)
                            identifierNameLengthBytes[index] = code[pointer + index];
                        pointer += (int)sizeof(identifierNameLengthBytes);
                        int idNameLength = convertBytesToInt(identifierNameLengthBytes);
                        char idNameBytes[idNameLength];
                        for (int index = 0; index < idNameLength; index++)
                            idNameBytes[index] = code[pointer + index];
                        pointer += idNameLength;
                        char* idName = static_cast<char *>(custom_alloc(strlen(idNameBytes) + 1));
                        strcpy(idName, idNameBytes);
                        auto* id = static_cast<Identifier *>(custom_alloc(sizeof(Identifier)));
                        id->id = idName;
                        inheritance->append(inheritance, id);
                    }
                    if (code[pointer] == 0x03) {
                        pointer++;
                        char behaviorCountBytes[4];
                        for (int index = 0; index < (int)sizeof(behaviorCountBytes); index++)
                            behaviorCountBytes[index] = code[pointer + index];
                        pointer += (int)sizeof(behaviorCountBytes);
                        int behaviorCount = convertBytesToInt(behaviorCountBytes);
                        auto* behavior = static_cast<List *>(custom_alloc(sizeof(struct List)));
                        initList(behavior);
                        for (int i = 0; i < behaviorCount; i++) {
                            char identifierNameLengthBytes[4];
                            for (int index = 0; index < (int)sizeof(identifierNameLengthBytes); index++)
                                identifierNameLengthBytes[index] = code[pointer + index];
                            pointer += (int)sizeof(identifierNameLengthBytes);
                            int idNameLength = convertBytesToInt(identifierNameLengthBytes);
                            char idNameBytes[idNameLength];
                            for (int index = 0; index < idNameLength; index++)
                                idNameBytes[index] = code[pointer + index];
                            pointer += idNameLength;
                            char* idName = &idNameBytes[0];
                            auto* id = static_cast<Identifier *>(custom_alloc(sizeof(Identifier)));
                            id->id = idName;
                            behavior->append(behavior, id);
                        }
                        if (code[pointer] == 0x04) {
                            pointer++;
                            char propCountBytes[4];
                            for (int index = 0; index < (int)sizeof(propCountBytes); index++)
                                propCountBytes[index] = code[pointer + index];
                            pointer += (int)sizeof(propCountBytes);
                            int propCount = convertBytesToInt(propCountBytes);
                            auto* props = static_cast<List *>(custom_alloc(sizeof(struct List)));
                            initList(props);
                            for (int i = 0; i < propCount; i++) {
                                char identifierNameLengthBytes[4];
                                for (int index = 0; index < (int)sizeof(identifierNameLengthBytes); index++)
                                    identifierNameLengthBytes[index] = code[pointer + index];
                                pointer += (int)sizeof(identifierNameLengthBytes);
                                int idNameLength = convertBytesToInt(identifierNameLengthBytes);
                                char idNameBytes[idNameLength];
                                for (int index = 0; index < idNameLength; index++)
                                    idNameBytes[index] = code[pointer + index];
                                pointer += idNameLength;
                                char* idName = static_cast<char *>(custom_alloc(strlen(idNameBytes) + 1));
                                strcpy(idName, idNameBytes);
                                auto* id = static_cast<Identifier *>(custom_alloc(sizeof(Identifier)));
                                id->id = idName;
                                id->exp.type = const_cast<char *>("Identifier");
                                id->exp.base.type = const_cast<char *>("Identifier");
                                char valueLengthBytes[4];
                                for (int index = 0; index < (int)sizeof(valueLengthBytes); index++)
                                    valueLengthBytes[index] = code[pointer + index];
                                pointer += (int)sizeof(valueLengthBytes);
                                auto valueLength = static_cast<unsigned long>(convertBytesToInt(
                                        valueLengthBytes));
                                char value[valueLength];
                                for (unsigned long i2 = pointer; i2 < pointer + valueLength; i2++)
                                    value[i2 - pointer] = code[i2];
                                pointer += valueLength;
                                Prop* prop = static_cast<Prop *>(custom_alloc(sizeof(Prop)));
                                prop->id = id;
                                prop->value = static_cast<char *>(custom_alloc(valueLength));
                                memcpy(prop->value, value, valueLength);
                                prop->loc = valueLength;
                                props->append(props, prop);
                            }
                            if (code[pointer] == 0x05) {
                                pointer++;
                                char funcCountBytes[4];
                                for (int index = 0; index < (int)sizeof(funcCountBytes); index++)
                                    funcCountBytes[index] = code[pointer + index];
                                pointer += (int)sizeof(funcCountBytes);
                                int funcCount = convertBytesToInt(funcCountBytes);
                                Dictionary* funcs = dict_new();
                                for (int i = 0; i < funcCount; i++) {
                                    if (code[pointer] == 0x51) {
                                        pointer++;
                                        auto* function = static_cast<Function *>(custom_alloc(sizeof(Function)));
                                        function->base.type = const_cast<char *>("Function");
                                        if (code[pointer] == 0x01) {
                                            pointer++;
                                            char funcNameLengthBytes[4];
                                            for (int index = 0; index < (int)sizeof(funcNameLengthBytes); index++)
                                                funcNameLengthBytes[index] = code[pointer + index];
                                            pointer += (int)sizeof(funcNameLengthBytes);
                                            int funcNameLength = convertBytesToInt(funcNameLengthBytes);
                                            char funcName[funcNameLength];
                                            for (int index = 0; index < funcNameLength; index++)
                                                funcName[index] = code[pointer + index];
                                            pointer += funcNameLength;
                                            char* funcNameStr = static_cast<char *>(custom_alloc(sizeof(funcName)));
                                            strcpy(funcNameStr, funcName);
                                            if (code[pointer] == 0x02) {
                                                pointer++;
                                                char funcLevelLengthBytes[4];
                                                for (int index = 0; index < (int)sizeof(funcLevelLengthBytes); index++)
                                                    funcLevelLengthBytes[index] = code[pointer + index];
                                                pointer += (int)sizeof(funcLevelLengthBytes);
                                                int funcLevelLength = convertBytesToInt(funcLevelLengthBytes);
                                                char funcLevelStr[funcLevelLength];
                                                for (int index = 0; index < funcLevelLength; index++)
                                                    funcLevelStr[index] = code[pointer + index];
                                                pointer += funcLevelLength;
                                                if (code[pointer] == 0x03) {
                                                    pointer++;
                                                    auto* identifiers = static_cast<List *>(custom_alloc(
                                                            sizeof(struct List)));
                                                    initList(identifiers);
                                                    char paramsCountBytes[4];
                                                    for (int index = 0; index < 4; index++)
                                                        paramsCountBytes[index] = code[pointer + index];
                                                    pointer += 4;
                                                    int paramsCount = convertBytesToInt(paramsCountBytes);
                                                    for (int counter = 0; counter < paramsCount; counter++) {
                                                        char paramLengthBytes[4];
                                                        for (int counter2 = 0; counter2 < (int)sizeof(paramLengthBytes); counter2++)
                                                            paramLengthBytes[counter2] = code[pointer + counter2];
                                                        pointer += (int)sizeof(paramLengthBytes);
                                                        int paramLength = convertBytesToInt(paramLengthBytes);
                                                        char idStr[paramLength];
                                                        for (int index = 0; index < paramLength; index++)
                                                            idStr[index] = code[pointer + index];
                                                        pointer += paramLength;
                                                        char* id = &idStr[0];
                                                        identifiers->append(identifiers, id);
                                                    }
                                                    if (code[pointer] == 0x04) {
                                                        pointer++;
                                                        if (code[pointer] == 0x6f) {
                                                            pointer++;
                                                            char jumpBytes[4];
                                                            for (int index = 0; index < (int)sizeof(jumpBytes); index++)
                                                                jumpBytes[index] = code[pointer + index];
                                                            pointer += (int)sizeof(jumpBytes);
                                                            int jump = convertBytesToInt(jumpBytes);
                                                            char body[jump];
                                                            for(unsigned long counter2 = pointer; counter2 < pointer + jump; counter2++)
                                                                body[counter2 - pointer] = code[counter2];
                                                            pointer += jump;
                                                            if (code[pointer] == 0x6e) {
                                                                pointer++;
                                                                function->base.type = const_cast<char *>("Function");
                                                                function->funcName = funcNameStr;
                                                                function->params = identifiers;
                                                                function->codes = static_cast<char *>(custom_alloc(
                                                                        static_cast<size_t>(jump)));
                                                                memcpy(function->codes, body, static_cast<size_t>(jump));
                                                                function->loc = static_cast<unsigned long>(jump);
                                                                dict_add(funcs, funcNameStr, function);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (code[pointer] == 0x05) {
                                    auto* constructor = static_cast<Constructor *>(custom_alloc(sizeof(Constructor)));
                                    pointer++;
                                    char paramsCountBytes[4];
                                    for (int i = 0; i < (int) sizeof(paramsCountBytes); i++)
                                        paramsCountBytes[i] = code[pointer + i];
                                    pointer += (int) sizeof(paramsCountBytes);
                                    int paramsCount = convertBytesToInt(paramsCountBytes);
                                    auto* ids = static_cast<List *>(custom_alloc(sizeof(struct List)));
                                    initList(ids);
                                    for (int counter = 0; counter < paramsCount; counter++) {
                                        char idNameLengthBytes[4];
                                        for (int i = 0; i < (int) sizeof(idNameLengthBytes); i++)
                                            idNameLengthBytes[i] = code[pointer + i];
                                        pointer += (int) sizeof(idNameLengthBytes);
                                        int idNameLength = convertBytesToInt(idNameLengthBytes);
                                        char idNameBytes[idNameLength];
                                        for (int i = 0; i < idNameLength; i++)
                                            idNameBytes[i] = code[pointer + i];
                                        pointer += idNameLength;
                                        char* idName = static_cast<char *>(custom_alloc(strlen(idNameBytes) + 1));
                                        strcpy(idName, idNameBytes);
                                        auto* id = static_cast<Identifier *>(custom_alloc(sizeof(Identifier)));
                                        id->exp.type = const_cast<char *>("Identifier");
                                        id->exp.base.type = const_cast<char *>("Identifier");
                                        id->id = idName;
                                        ids->append(ids, id);
                                    }
                                    constructor->params = ids;
                                    if (code[pointer] == 0x06) {
                                        pointer++;
                                        if (code[pointer] == 0x6f) {
                                            pointer++;
                                            char bodyLengthBytes[4];
                                            for (int i = 0; i < (int) sizeof(bodyLengthBytes); i++)
                                                bodyLengthBytes[i] = code[pointer + i];
                                            pointer += (int) sizeof(bodyLengthBytes);
                                            int bodyLength = convertBytesToInt(bodyLengthBytes);
                                            char body[bodyLength];
                                            for (int i = 0; i < bodyLength; i++)
                                                body[i] = code[pointer + i];
                                            pointer += bodyLength;
                                            constructor->loc = static_cast<unsigned long>(bodyLength);
                                            if (code[pointer] == 0x6e) {
                                                pointer++;
                                                constructor->body = static_cast<char *>(custom_alloc(
                                                        static_cast<size_t>(bodyLength)));
                                                memcpy(constructor->body, body, static_cast<size_t>(bodyLength));
                                            }
                                        }
                                    }
                                    classObj->inheritance = inheritance;
                                    classObj->behavior = behavior;
                                    classObj->properties = props;
                                    classObj->functions = funcs;
                                    classObj->className = classNameStr;
                                    classObj->constructor = constructor;
                                    dict_add((Dictionary *) dataStack.top(&dataStack), className, classObj);
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (code[pointer] == 0x52) {
            pointer++;
            bool matched = false;
            if (code[pointer] == 0x01) {
                pointer++;
                Dictionary* expDict = dict_new();
                expStack.push(&expStack, expDict);
                calculate(1);
                auto* condition = (BoolValue*)dict_get((Dictionary*)expStack.pop(&expStack),
                                                            const_cast<char *>("value"));
                if (code[pointer] == 0x02) {
                    pointer++;
                    if (code[pointer] == 0x6f) {
                        pointer++;
                        char jumpBytes[4];
                        for (int index = 0; index < (int)sizeof(jumpBytes); index++)
                            jumpBytes[index] = code[pointer + index];
                        pointer += (int)sizeof(jumpBytes);
                        int jump = convertBytesToInt(jumpBytes);
                        char body[jump];
                        for(unsigned long i = pointer; i < pointer + jump; i++)
                            body[i - pointer] = code[i];
                        pointer += jump;
                        if (condition->value) {
                            matched = true;
                            void* retVal = executeIntern(body, static_cast<unsigned long>(jump), nullptr);
                            if (retVal != nullptr && strcmp(((char*)retVal), "break") == 0) {
                                break;
                            } else if (retVal != nullptr && strcmp(((char*)retVal), "break") != 0) {
                                return retVal;
                            }
                        }
                        if (code[pointer] == 0x6e) {
                            pointer++;
                            if (code[pointer] == 0x03) {
                                pointer++;
                                char elseCountLength[4];
                                for (int index = 0; index < 4; index++)
                                    elseCountLength[index] = code[pointer + index];
                                pointer += 4;
                                int elseCount = convertBytesToInt(elseCountLength);
                                for (int elseCounter = 0; elseCounter < elseCount; elseCounter++) {
                                    if (code[pointer] == 0x53) {
                                        pointer++;
                                        if (code[pointer] == 0x01) {
                                            pointer++;
                                            Dictionary* expDict2 = dict_new();
                                            expStack.push(&expStack, expDict2);
                                            calculate(1);
                                            auto* elseCondition = (BoolValue*)dict_get((Dictionary*)expStack.pop(&expStack),
                                                                                            const_cast<char *>("value"));
                                            if (code[pointer] == 0x02) {
                                                pointer++;
                                                if (code[pointer] == 0x6f) {
                                                    pointer++;
                                                    char jumpBytes2[4];
                                                    for (int index = 0; index < (int)sizeof(jumpBytes2); index++)
                                                        jumpBytes2[index] = code[pointer + index];
                                                    pointer += (int)sizeof(jumpBytes2);
                                                    int jump2 = convertBytesToInt(jumpBytes2);
                                                    char body2[jump2];
                                                    for(unsigned long i = pointer; i < pointer + jump2; i++)
                                                        body2[i - pointer] = code[i];
                                                    pointer += jump2;
                                                    if (!matched && elseCondition->value) {
                                                        matched = true;
                                                        executeIntern(body2, static_cast<unsigned long>(jump2), nullptr);
                                                    }
                                                    if (code[pointer] == 0x6e) {
                                                        pointer++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (code[pointer] == 0x54) {
                                        pointer++;
                                        if (code[pointer] == 0x01) {
                                            pointer++;
                                            if (code[pointer] == 0x6f) {
                                                pointer++;
                                                char jumpBytes2[4];
                                                for (int index = 0; index < (int)sizeof(jumpBytes2); index++)
                                                    jumpBytes2[index] = code[pointer + index];
                                                pointer += (int)sizeof(jumpBytes2);
                                                int jump2 = convertBytesToInt(jumpBytes2);
                                                char body2[jump2];
                                                for(unsigned long i = pointer; i < pointer + jump2; i++)
                                                    body2[i - pointer] = code[i];
                                                pointer += jump2;
                                                if (!matched)
                                                    executeIntern(body2, static_cast<unsigned long>(jump2), nullptr);
                                                if (code[pointer] == 0x6e) {
                                                    pointer++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (code[pointer] == 0x53) {
            pointer++;
            unsigned long bodyStartPos = pointer;
            void* counter = nullptr;
            while (true) {
                pointer = bodyStartPos;
                if (code[pointer] == 0x01) {
                    pointer++;
                    expStack.push(&expStack, dict_new());
                    calculate(1);
                    void* limitRaw = dict_get((Dictionary*)expStack.pop(&expStack), const_cast<char *>("value"));
                    if (code[pointer] == 0x02) {
                        pointer++;
                        expStack.push(&expStack, dict_new());
                        calculate(1);
                        void* stepRaw = dict_get((Dictionary*)expStack.pop(&expStack), const_cast<char *>("value"));
                        if (code[pointer] == 0x03) {
                            pointer++;
                            if (code[pointer] == 0x6f) {
                                pointer++;
                                char jumpBytes[4];
                                for (int index = 0; index < (int)sizeof(jumpBytes); index++)
                                    jumpBytes[index] = code[pointer + index];
                                pointer += (int)sizeof(jumpBytes);
                                int jump = convertBytesToInt(jumpBytes);
                                char body[jump];
                                for(unsigned long i = pointer; i < pointer + jump; i++)
                                    body[i - pointer] = code[i];
                                pointer += jump;
                                if (counter == nullptr) {
                                    if (((Value *) limitRaw)->valueType == 0x02) {
                                        counter = custom_alloc(sizeof(ShortValue));
                                        ((ShortValue *) counter)->value = 0;
                                        ((ShortValue *) counter)->base.valueType = 0x02;
                                    } else if (((Value *) limitRaw)->valueType == 0x06) {
                                        counter = custom_alloc(sizeof(IntValue));
                                        ((IntValue *) counter)->value = 0;
                                        ((ShortValue *) counter)->base.valueType = 0x06;
                                    } else if (((Value *) limitRaw)->valueType == 0x03) {
                                        counter = custom_alloc(sizeof(LongValue));
                                        ((LongValue *) counter)->value = 0;
                                        ((ShortValue *) counter)->base.valueType = 0x03;
                                    } else if (((Value *) limitRaw)->valueType == 0x04) {
                                        counter = custom_alloc(sizeof(FloatValue));
                                        ((FloatValue *) counter)->value = 0;
                                        ((ShortValue *) counter)->base.valueType = 0x04;
                                    } else if (((Value *) limitRaw)->valueType == 0x05) {
                                        counter = custom_alloc(sizeof(DoubleValue));
                                        ((DoubleValue *) counter)->value = 0;
                                        ((ShortValue *) counter)->base.valueType = 0x05;
                                    }
                                }
                                if (((BoolValue*)lt(counter, limitRaw))->value) {
                                    void* retVal = executeIntern(body, static_cast<unsigned long>(jump), nullptr);
                                    if (retVal != nullptr && strcmp(((char*)retVal), "break") == 0) {
                                        break;
                                    } else if (retVal != nullptr && strcmp(((char*)retVal), "break") != 0) {
                                        return retVal;
                                    }
                                    counter = sum(counter, stepRaw);
                                }
                                else {
                                    if (code[pointer] == 0x6e) {
                                        pointer++;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (code[pointer] == 0x54) {
            pointer++;
            unsigned long bodyStartPos = pointer;
            while (true) {
                pointer = bodyStartPos;
                if (code[pointer] == 0x01) {
                    pointer++;
                    expStack.push(&expStack, dict_new());
                    calculate(1);
                    void *condition = dict_get((Dictionary *) expStack.pop(&expStack), const_cast<char *>("value"));
                    if (code[pointer == 0x02]) {
                        pointer++;
                        if (code[pointer] == 0x6f) {
                            pointer++;
                            char jumpBytes[4];
                            for (int index = 0; index < (int)sizeof(jumpBytes); index++)
                                jumpBytes[index] = code[pointer + index];
                            pointer += (int)sizeof(jumpBytes);
                            int jump = convertBytesToInt(jumpBytes);
                            char body[jump];
                            for(unsigned long i = pointer; i < pointer + jump; i++)
                                body[i - pointer] = code[i];
                            pointer += jump;
                            if (((BoolValue *) condition)->value) {
                                void* retVal = executeIntern(body, static_cast<unsigned long>(jump), nullptr);
                                if (retVal != nullptr && strcmp(((char*)retVal), "break") == 0) {
                                    break;
                                } else if (retVal != nullptr && strcmp(((char*)retVal), "break") != 0) {
                                    return retVal;
                                }
                            } else {
                                if (code[pointer] == 0x6e) {
                                    pointer++;
                                }
                                break;
                            }
                            if (code[pointer] == 0x6e) {
                                pointer++;
                            }
                        }
                    }
                }
            }
        }
        else if (code[pointer] == 0x55) {
            pointer++;
            if (code[pointer] == 0x01) {
                pointer++;
                expStack.push(&expStack, dict_new());
                calculate(1);
                void* target = dict_get((Dictionary*)expStack.top(&expStack), const_cast<char *>("value"));
                char* refStr = static_cast<char *>(dict_get((Dictionary *) expStack.top(&expStack),
                                                            const_cast<char *>("value2")));
                auto* thisObj = static_cast<Object *>(dict_get((Dictionary *) expStack.pop(&expStack),
                                                                 const_cast<char *>("value3")));
                if (code[pointer] == 0x02) {
                    pointer++;
                    char entriesCountBytes[4];
                    for (int index = 0; index < 4; index++)
                        entriesCountBytes[index] = code[pointer + index];
                    pointer += 4;
                    int entriesCount = convertBytesToInt(entriesCountBytes);
                    Dictionary* entriesDict = dict_new();
                    for (int counter = 0; counter < entriesCount; counter++) {
                        if (code[pointer] == 0x03) {
                            pointer++;
                            char keyLengthBytes[4];
                            for (int index = 0; index < 4; index++)
                                keyLengthBytes[index] = code[pointer + index];
                            pointer += 4;
                            int keyLength = convertBytesToInt(keyLengthBytes);
                            char keyBytes[keyLength];
                            for (int index = 0; index < keyLength; index++)
                                keyBytes[index] = code[pointer + index];
                            pointer += keyLength;
                            char *key = static_cast<char *>(custom_alloc(strlen(keyBytes) + 1));
                            strcpy(key, keyBytes);
                            char valueLengthBytes[4];
                            for (int index = 0; index < 4; index++)
                                valueLengthBytes[index] = code[pointer + index];
                            pointer += 4;
                            expStack.push(&expStack, dict_new());
                            calculate(1);
                            void* value = dict_get((Dictionary *) expStack.pop(&expStack), const_cast<char *>("value"));
                            dict_add(entriesDict, key, value);
                        }
                    }
                    if (target == nullptr) {
                        routeAndResolve(refStr, entriesDict);
                    } else {
                        auto* func = (Function*) target;
                        if (thisObj != nullptr)
                            dict_add(entriesDict, const_cast<char *>("this"), thisObj);
                        executeIntern(func->codes, func->loc, entriesDict);
                    }
                }
            }
        }
        else if (code[pointer] == 0x56) {
            pointer++;
            if (code[pointer] == 0x01) {
                pointer++;
                expStack.push(&expStack, dict_new());
                calculate(0);
                auto* ref = (struct Reference*)dict_get((Dictionary*)expStack.pop(&expStack),
                                                                    const_cast<char *>("value"));
                if (code[pointer] == 0x02) {
                    pointer++;
                    expStack.push(&expStack, dict_new());
                    calculate(1);
                    void* exp = dict_get((Dictionary*)expStack.pop(&expStack), const_cast<char *>("value"));
                    Object* objectChain = nullptr;
                    char* varNameStr = static_cast<char *>(custom_alloc(strlen(ref->currentChain->id) + 1));
                    strcpy(varNameStr, ref->currentChain->id);
                    struct StackDataItem* iterator = dataStack.item;
                    void* variable = dict_get(((Dictionary*)iterator->data), varNameStr);
                    if (variable != nullptr && strcmp(((Code*)variable)->type, "Object") == 0)
                        objectChain = static_cast<Object *>(variable);
                    while (variable == nullptr && iterator->prev != nullptr) {
                        iterator = iterator->prev;
                        variable = dict_get((Dictionary*)iterator->data, varNameStr);
                    }
                    Identifier* finalChain = ref->currentChain;
                    ref = static_cast<Reference *>(ref->restOfTheChain);
                    char* refStr = varNameStr;
                    while (variable != nullptr && ref != nullptr) {
                        if (strcmp(((Code*)variable)->type, "Object") == 0) {
                            void* variable2 = dict_get(((Object*)variable)->value, ref->currentChain->id);
                            if (variable == nullptr) {
                                variable2 = dict_get(((Object *) variable)->funcs, ref->currentChain->id);
                                if (variable2 != nullptr)
                                    variable = variable2;
                            }
                            else
                                variable = variable2;
                        }
                        else if (strcmp(((Code*)variable)->type, "Class") == 0) {
                            variable = dict_get(((Class*)variable)->functions, ref->currentChain->id);
                        }
                        strcat(refStr, ".");
                        strcat(refStr, ref->currentChain->id);
                        finalChain = ref->currentChain;
                        ref = static_cast<Reference *>(ref->restOfTheChain);
                        if (variable != nullptr && strcmp(((Code*)variable)->type, "Object") == 0)
                            objectChain = static_cast<Object *>(variable);
                    }
                    if (objectChain != nullptr) {
                        char *varNameStr2 = static_cast<char *>(custom_alloc(strlen(finalChain->id) + 1));
                        strcpy(varNameStr2, finalChain->id);
                        dict_delete(objectChain->value, varNameStr2);
                        dict_add(objectChain->value, varNameStr2, exp);
                    } else {
                        if (variable == nullptr) {
                            char *varNameStr2 = static_cast<char *>(custom_alloc(strlen(finalChain->id) + 1));
                            strcpy(varNameStr2, finalChain->id);
                            dict_add(((Dictionary *) dataStack.top(&dataStack)), varNameStr2, exp);
                            notifyNewUsage(exp);
                        } else {
                            char *varNameStr2 = static_cast<char *>(custom_alloc(strlen(finalChain->id) + 1));
                            strcpy(varNameStr2, finalChain->id);
                            dict_delete((Dictionary *) iterator->data, varNameStr2);
                            dict_add((Dictionary *) iterator->data, varNameStr2, exp);
                        }
                    }
                }
            }
        }
        else if (code[pointer] == 0x59) {
            pointer++;
            if (code[pointer] == 0x01) {
                pointer++;
                expStack.push(&expStack, dict_new());
                calculate(1);
                return dict_get((Dictionary*)expStack.pop(&expStack), const_cast<char *>("value"));
            }
        }
    }
    return nullptr;
}

void* executeIntern(char c[], unsigned long length, Dictionary* entriesDict) {
    if (entriesDict == nullptr)
        dataStack.push(&dataStack, dict_new());
    else
        dataStack.push(&dataStack, entriesDict);
    bufferStack.push(&bufferStack, dict_new());
    auto *cp = static_cast<CodePack *>(custom_alloc(sizeof(CodePack)));
    cp->code = code;
    cp->loc = codeLength;
    cp->pointer = pointer;
    cp->base.type = const_cast<char *>("CodePack");
    codeLengthStack.push(&codeLengthStack, cp);
    code = &c[0];
    codeLength = length;
    pointer = 0;
    void *returnValue = ride();
    cp = static_cast<CodePack *>(codeLengthStack.pop(&codeLengthStack));
    code = cp->code;
    codeLength = cp->loc;
    pointer = cp->pointer;
    bufferStack.pop(&bufferStack);
    dataStack.pop(&dataStack);
    return returnValue;
}

void execute(char* c, long length) {
    initStack(&codeLengthStack);
    initStack(&bufferStack);
    initStack(&expStack);
    initStack(&dataStack);
    executeIntern(c, static_cast<unsigned long>(length), nullptr);
}