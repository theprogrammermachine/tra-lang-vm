
#include <cfloat>
#include <ctime>
#include <cmath>
#include <uuid/uuid.h>
#include "../structures/stack/Stack.hpp"
#include "../api/IO/ConsolePrinter.hpp"
#include "../api/Cipher/Sha256.hpp"
#include "../api/String/String.hpp"
#include "../structures/array/array.hpp"

void* executeIntern(char c[], unsigned long length, Dictionary* entriesDict);
void calculate(int investigateId);
void* ride();
void execute(char* c, long length);
char* stringifyObject(Object* obj);
char* stringifyArray(Array* array);
void* multiply(void* value1, void* value2);