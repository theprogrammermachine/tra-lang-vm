
#include "GarbageCenter.hpp"

void notifyNewUsage(void* pointer) {
    ((Code*)pointer)->refCount++;
}

bool isValidForDelete(void* pointer) {
    return ((Code*)pointer)->refCount <= 0;
}

void freeList(struct List* list) {
    while (list->listPointer != nullptr) {
        struct ListDataItem* temp = list->listPointer;
        list->listPointer = list->listPointer->prev;
        if (isValidForDelete(temp->data)) free(temp->data);
        free(temp);
    }
}

void dict_free(Dictionary* d)
{
    int i;
    struct elt *e;
    struct elt *next;

    for(i = 0; i < d->size; i++) {
        for(e = d->table[i]; e != nullptr; e = next) {
            next = e->next;
            if (isValidForDelete(e->key))
                free(e->key);
            if (isValidForDelete(e->value))
                free(e->value);
            free(e);
        }
    }

    free(d->table);
    free(d);
}

void freeData(void* data) {
    if (strcmp(((Code*)data)->type, "Reference") == 0) {
        free(((struct Reference*)data)->base.type);
        freeData(((struct Reference*)data)->currentChain);
        freeData(((struct Reference*)data)->restOfTheChain);
    }
    else if (strcmp(((Code*)data)->type, "Identifier") == 0) {
        free(((Identifier*)data)->exp.base.type);
        free(((Identifier*)data)->exp.type);
        free(((Identifier*)data)->id);
    }
    else if (strcmp(((Code*)data)->type, "Index") == 0) {
        free(((struct Index*)data)->base.type);
        freeData(((struct Index*)data)->var);
        freeData(((struct Index*)data)->restOfTheChain);
        freeList(((struct Index*)data)->index);
    }
    else if (strcmp(((Code*)data)->type, "Dictionary") == 0) {
        dict_free((Dictionary*)data);
    }
    else if (strcmp(((Code*)data)->type, "Value") == 0) {
        free(((Value*)data)->exp.base.type);
        free(((Value*)data)->exp.type);
    }
    free(data);
}

void notifyUsageEnded(void* pointer) {
    ((Code*)pointer)->refCount--;
    if (((Code*)pointer)->refCount <= 0)
        freeData(pointer);
}