
#include <cstdlib>
#include <cstring>
#include "../structures/dictionary/Dictionary.hpp"

void notifyNewUsage(void* pointer);
void notifyUsageEnded(void* pointer);
void flagAsGarbage(void* garbage);
void unFlagGarbage(void* garbage);
void destroyAllGarbage();