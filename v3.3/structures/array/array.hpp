
#include "../../utils/GarbageCenter.hpp"
#include <cstddef>

typedef struct {
    Code base;
    void** array{};
    size_t used{};
    size_t size{};
} Array;

void initArray(Array *a, size_t initialSize);
void insertArray(Array *a, void* element);
void freeArray(Array *a);