
#include <cstdlib>
#include "array.hpp"

void initArray(Array *a, size_t initialSize) {
    a->array = (void**)malloc(initialSize * sizeof(void*));
    a->used = 0;
    a->size = initialSize;
}

void insertArray(Array *a, void* element) {
    if (a->used == a->size) {
        a->size += 100;
        a->array = (void**)realloc(a->array, a->size * sizeof(void*));
    }
    a->array[a->used] = element;
    a->used++;
}

void freeArray(Array *a) {
    free(a->array);
    a->array = nullptr;
    a->used = a->size = 0;
}