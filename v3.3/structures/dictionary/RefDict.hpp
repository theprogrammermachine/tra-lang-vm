
#include "../../models/Models.hpp"

RefDict* refdict_new();
void refdict_free(RefDict*);
void refdict_add(RefDict*, void *key, void *value);
void *refdict_get(RefDict*, void *key);
void refdisplay_keys(RefDict* d);
struct List* refToList(RefDict* d);
void refdict_delete(RefDict*, void *key);