#include <cstdlib>
#include <cassert>
#include <cstring>
#include <cstdio>

#include "RefDict.hpp"
#include "../list/List.hpp"
#include "../pair/Pair.hpp"

#define INITIAL_SIZE (256)
#define GROWTH_FACTOR (2)
#define MAX_LOAD_FACTOR (1)

/* RefDict initialization code used in both RefDictCreate and grow */
RefDict*
refInternalRefDictCreate(int size)
{
    RefDict* d;
    int i;

    d = static_cast<RefDict *>(malloc(sizeof(*d)));

    assert(d != nullptr);

    d->size = size;
    d->n = 0;
    d->table = static_cast<refelt **>(malloc(sizeof(struct refelt *) * d->size));

    for(i = 0; i < d->size; i++) d->table[i] = nullptr;

    d->base.type = const_cast<char*>("Dictionary");

    return d;
}

RefDict*
refdict_new()
{
    return refInternalRefDictCreate(INITIAL_SIZE);
}

void
refdict_free(RefDict* d)
{
    int i;
    struct refelt *e;
    struct refelt *next;

    for(i = 0; i < d->size; i++) {
        for(e = d->table[i]; e != nullptr; e = next) {
            next = e->next;

            free(e);
        }
    }

    free(d->table);
    free(d);
}

#define MULTIPLIER (97)

unsigned int hash(void* input) {
    auto x = *(char*) input;
    x = ((x >> 16) ^ x) * 0x45d9f3b;
    x = ((x >> 16) ^ x) * 0x45d9f3b;
    x = (x >> 16) ^ x;
    return x;
}

static void refgrow(RefDict* d)
{
    RefDict* d2;            /* new RefDictionary we'll create */
    RefDict* swap;   /* temporary structure for brain transplant */
    int i;
    struct refelt *e;

    d2 = refInternalRefDictCreate(d->size * GROWTH_FACTOR);

    for(i = 0; i < d->size; i++) {
        for(e = d->table[i]; e != nullptr; e = e->next) {
            /* note: this recopies everything */
            /* a more efficient implementation would
             * patch out the strdups inside RefDictInsert
             * to avoid this problem */
            refdict_add(d2, e->key, e->value);
        }
    }

    /* the hideous part */
    /* We'll swap the guts of d and d2 */
    /* then call RefDictDestroy on d2 */
    swap = d;
    *d = *d2;
    d2 = swap;

    refdict_free(d2);
}

/* insert a new key-value pair into an existing RefDict */
void
refdict_add(RefDict* d, void *key, void *value)
{
    struct refelt *e;
    unsigned long h;

    assert(key);

    e = static_cast<refelt *>(malloc(sizeof(*e)));

    assert(e);

    e->key = key;
    e->value = value;

    h = hash(key) % d->size;

    e->next = d->table[h];
    d->table[h] = e;

    d->n++;

    /* grow table if there is not enough room */
    if(d->n >= d->size * MAX_LOAD_FACTOR) {
        refgrow(d);
    }
}

/* return the most recently inserted value associated with a key */
/* or 0 if no matching key is present */
void *refdict_get(RefDict* d, void *key)
{
    struct refelt *e;

    for(e = d->table[hash(key) % d->size]; e != nullptr; e = e->next) {
        if(e->key == key) {
            return e->value;
        }
    }

    return nullptr;
}

void
refdisplay_keys(RefDict* d) {
    printf("\n==================================================\n");
    for(int i = 0; i < d->size; i++) {
        auto *e = reinterpret_cast<elt *>(d->table[i]);
        while (e != nullptr) {
            printf("\nkey : %sd\n", e->key);
            e = e->next;
        }
    }
    printf("\n==================================================\n");
}

struct List* refToList(RefDict* d) {
    auto* result = static_cast<List *>(malloc(sizeof(struct List)));
    initList(result);
    for(int i = 0; i < d->size; i++) {
        struct refelt *e = d->table[i];
        while (e != nullptr) {
            auto pair = static_cast<Pair *>(malloc(sizeof(Pair)));
            pair->first = e->key;
            pair->second = e->value;
            result->append(result, pair);
            e = e->next;
        }
    }
    return result;
}

/* delete the most recently inserted record with the given key */
/* if there is no such record, has no effect */
void
refdict_delete(RefDict* d, void *key)
{
    struct refelt **prev;          /* what to change when elt is deleted */
    struct refelt *e;              /* what to delete */

    for(prev = &(d->table[hash(key) % d->size]);
        *prev != nullptr;
        prev = &((*prev)->next)) {
        if((*prev)->key == key) {
            /* got it */
            e = *prev;
            *prev = e->next;

            free(e->key);
            free(e);

            d->n--;

            return;
        }
    }
}