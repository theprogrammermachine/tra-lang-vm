#include <cstdlib>
#include <cassert>
#include <cstring>
#include <cstdio>

#include "Dictionary.hpp"

#define INITIAL_SIZE (256)
#define GROWTH_FACTOR (2)
#define MAX_LOAD_FACTOR (1)

/* Dictionary initialization code used in both DictionaryCreate and grow */
Dictionary*
internalDictionaryCreate(int size)
{
    auto* d = static_cast<Dictionary *>(malloc(sizeof(Dictionary *)));
    int i;

    d->size = size;
    d->n = 0;
    d->table = static_cast<elt **>(malloc(sizeof(struct elt *) * d->size));

    for(i = 0; i < d->size; i++) d->table[i] = nullptr;

    d->base.type = const_cast<char *>("Dictionary");

    return d;
}

Dictionary*
dict_new()
{
    return internalDictionaryCreate(INITIAL_SIZE);
}

#define MULTIPLIER (97)

static unsigned long
hash_function(const char *s)
{
    unsigned const char *us;
    unsigned long h;

    h = 0;

    for(us = (unsigned const char *) s; *us; us++) {
        h = h * MULTIPLIER + *us;
    }

    return h;
}

static void
grow(Dictionary* d)
{
    Dictionary* d2;            /* new Dictionaryionary we'll create */
    Dictionary* swap;   /* temporary structure for brain transplant */
    int i;
    struct elt *e;

    d2 = internalDictionaryCreate(d->size * GROWTH_FACTOR);

    for(i = 0; i < d->size; i++) {
        for(e = d->table[i]; e != nullptr; e = e->next) {
            /* note: this recopies everything */
            /* a more efficient implementation would
             * patch out the strdups inside DictionaryInsert
             * to avoid this problem */
            dict_add(d2, e->key->value, e->value);
        }
    }

    /* the hideous part */
    /* We'll swap the guts of d and d2 */
    /* then call DictionaryDestroy on d2 */
    swap = d;
    *d = *d2;
    d2 = swap;

    dict_free(d2);
}

/* insert a new key-value pair into an existing Dictionary */
void
dict_add(Dictionary* d, char *key, void *value)
{
    struct elt *e;
    unsigned long h;

    assert(key);

    e = static_cast<elt *>(malloc(sizeof(*e)));

    assert(e);

    e->key = static_cast<StringValue *>(malloc(sizeof(StringValue *)));
    e->key->value = static_cast<char *>(malloc(strlen(key) + 1));
    e->key->value = strdup(key);
    e->value = value;

    h = hash_function(key) % d->size;

    e->next = d->table[h];
    d->table[h] = e;

    d->n++;

    /* grow table if there is not enough room */
    if(d->n >= d->size * MAX_LOAD_FACTOR) {
        grow(d);
    }
}

/* return the most recently inserted value associated with a key */
/* or 0 if no matching key is present */
void *
dict_get(Dictionary* d, char *key)
{
    struct elt *e;

    for(e = d->table[hash_function(key) % d->size]; e != NULL; e = e->next) {
        if(!strcmp(e->key->value, key)) {
            return e->value;
        }
    }

    return NULL;
}

void
display_keys(Dictionary* d) {
    printf("\n==================================================\n");
    for(int i = 0; i < d->size; i++) {
        struct elt *e = d->table[i];
        while (e != nullptr) {
            printf("\nkey : %s\n", e->key->value);
            e = e->next;
        }
    }
    printf("\n==================================================\n");
}

struct List* toList(Dictionary* d) {
    auto* result = static_cast<List *>(malloc(sizeof(struct List)));
    initList(result);
    for(int i = 0; i < d->size; i++) {
        struct elt *e = d->table[i];
        while (e != nullptr) {
            Pair* pair = static_cast<Pair *>(malloc(sizeof(Pair)));
            pair->first = e->key;
            pair->second = e->value;
            result->append(result, pair);
            e = e->next;
        }
    }
    return result;
}

void
dict_delete(Dictionary* d, const char *key)
{
    struct elt **prev;
    struct elt *e;

    for(prev = &(d->table[hash_function(key) % d->size]);
        *prev != nullptr;
        prev = &((*prev)->next)) {
        if(!strcmp((*prev)->key->value, key)) {
            e = *prev;
            *prev = e->next;

            free(e->key->value);
            free(e->key);
            free(e);

            d->n--;

            return;
        }
    }
}