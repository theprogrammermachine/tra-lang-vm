
#include "Pair.hpp"

Pair* newPair(void* first, void* second) {
    Pair* pair = static_cast<Pair *>(malloc(sizeof(Pair)));
    pair->first = first;
    pair->second = second;
    return pair;
}