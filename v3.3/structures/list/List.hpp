

#include <cstdlib>

struct ListDataItem {
    void* data;
    struct ListDataItem* prev;
    struct ListDataItem* next;
};

void initList(struct List* list);
void listAdd(struct List* list, void* item);
void* iteratorForward(struct List* list);
void* iteratorBackward(struct List* list);
bool iteratorHasNext(struct List* list);
bool iteratorHasBefore(struct List* list);
void freeList(struct List* list);