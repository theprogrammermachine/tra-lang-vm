

#include <cstdio>
#include <cstring>
#include <cstdint>

struct StackDataItem {
    void* data;
    struct StackDataItem* prev;
};

void push(struct Stack* stack ,void* data);
struct StackDataItem* iterator(struct Stack* stack);
void* top(struct Stack* stack);
void* pop(struct Stack* stack);
int size(struct Stack* stack);
bool isEmpty(struct Stack* stack);
