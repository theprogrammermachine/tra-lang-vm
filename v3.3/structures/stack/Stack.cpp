
#include <cstdlib>
#include "Stack.hpp"
#include "../../models/Models.hpp"

void push(struct Stack* stack ,void* data) {
    auto* pItem = static_cast<StackDataItem *>(malloc(sizeof(struct StackDataItem)));
    pItem->data = data;
    pItem->prev = stack->item;
    stack->item = pItem;
    stack->stackSize++;
}

struct StackDataItem* iterator(struct Stack* stack) {
    struct StackDataItem* iterator = stack->item;
    return iterator;
}

void* top(struct Stack* stack) {
    if (stack->item == nullptr)
        return nullptr;
    return stack->item->data;
}

void* pop(struct Stack* stack) {
    if (stack->item == nullptr)
        return nullptr;
    void* topData = stack->item->data;
    stack->item = stack->item->prev;
    stack->stackSize--;
    return topData;
}

int size(struct Stack* stack) {
    return stack->stackSize;
}

bool isEmpty(struct Stack* stack) {
    return stack->stackSize == 0;
}