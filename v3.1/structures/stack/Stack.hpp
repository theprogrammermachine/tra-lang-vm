
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

struct StackDataItem {
    void* data;
    struct StackDataItem* prev;
};

struct Stack {
    int stackSize;
    struct StackDataItem* item;
    void (*push)(struct Stack*, void*);
    void* (*top)(struct Stack*);
    void* (*pop)(struct Stack*);
    int (*size)(struct Stack*);
    bool (*isEmpty)(struct Stack*);
    struct StackDataItem* (*iterator)(struct Stack*);
};

void push(struct Stack* stack ,void* data);
struct StackDataItem* iterator(struct Stack* stack);
void* top(struct Stack* stack);
void* pop(struct Stack* stack);
int size(struct Stack* stack);
bool isEmpty(struct Stack* stack);
