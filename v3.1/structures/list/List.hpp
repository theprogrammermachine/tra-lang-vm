
#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>

struct ListDataItem {
    void* data;
    struct ListDataItem* prev;
    struct ListDataItem* next;
};

struct List {
    int size;
    struct ListDataItem* listPointer;
    struct ListDataItem* iteratorPointer;
    void  (*append)(struct List*, void*);
    void* (*iteratorForward)(struct List*);
    void* (*iteratorBackward)(struct List*);
    bool  (*iteratorHasNext)(struct List*);
    bool  (*iteratorHasBefore)(struct List*);
};

void initList(struct List* list);
void listAdd(struct List* list, void* item);
void* iteratorForward(struct List* list);
void* iteratorBackward(struct List* list);
bool iteratorHasNext(struct List* list);
bool iteratorHasBefore(struct List* list);