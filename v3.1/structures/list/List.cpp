
#include "List.hpp"

void initList(struct List* list) {
    list->size = 0;
    list->listPointer = nullptr;
    list->iteratorPointer = nullptr;
    list->append = listAdd;
    list->iteratorForward = iteratorForward;
    list->iteratorHasNext = iteratorHasNext;
    list->iteratorBackward = iteratorBackward;
    list->iteratorHasBefore = iteratorHasBefore;
}

void listAdd(struct List* list, void* item) {
    auto* pItem = static_cast<ListDataItem *>(malloc(sizeof(struct ListDataItem)));
    pItem->data = item;
    if (list->listPointer != nullptr) {
        pItem->prev = list->listPointer;
        list->listPointer->next = pItem;
    }
    if (list->iteratorPointer == nullptr) {
        list->iteratorPointer = pItem;
        list->iteratorPointer->next = nullptr;
        list->iteratorPointer->prev = nullptr;
    }
    list->listPointer = pItem;
    list->size++;
}

void* iteratorForward(struct List* list) {
    void* data = list->iteratorPointer->data;
    list->iteratorPointer = list->iteratorPointer->next;
    return data;
}

void* iteratorBackward(struct List* list) {
    void* data = list->iteratorPointer->data;
    list->iteratorPointer = list->iteratorPointer->prev;
    return data;
}

bool iteratorHasNext(struct List* list) {
    return (list->iteratorPointer != nullptr);
}

bool iteratorHasBefore(struct List* list) {
    return (list->iteratorPointer != nullptr);
}