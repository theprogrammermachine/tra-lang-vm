
#include "Sha256.hpp"

char* sha256_hex(char* s) {
    SHA256_CTX ctx;
    u_int8_t results[SHA256_DIGEST_LENGTH];
    char *buf = s;
    int n;
    n = strlen(buf);
    SHA256_Init(&ctx);
    SHA256_Update(&ctx, (u_int8_t *)buf, n);
    SHA256_Final(results, &ctx);
    char* finalResult = static_cast<char *>(malloc(SHA256_DIGEST_LENGTH * 2));
    for (n = 0; n < SHA256_DIGEST_LENGTH; n++)
        sprintf(finalResult + n * 2, "%02x", results[n]);
    return finalResult;
}