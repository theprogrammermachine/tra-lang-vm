
#include <cfloat>
#include <ctime>
#include <cmath>
#include "../structures/array/array.hpp"
#include "../api/IO/ConsolePrinter.hpp"

void* executeIntern(char c[], unsigned long length, Dictionary* entriesDict);
void calculate(int investigateId);
void* ride();
void execute(char* c, long length);