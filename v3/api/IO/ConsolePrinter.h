
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct {
    void* (*print)(char* text);
} ConsolePrinter;

void* print(char* text);
ConsolePrinter createConsolePrinter();