
#include <float.h>
#include <sys/time.h>
#include <math.h>
#include "../structures/array/array.h"
#include "../api/IO/ConsolePrinter.h"

void* executeIntern(char c[], unsigned long length, Dictionary* entriesDict);
void calculate(int investigateId);
void* ride();
void execute(char* c, long length);