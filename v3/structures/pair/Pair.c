
#include "Pair.h"

Pair* newPair(void* first, void* second) {
    Pair* pair = malloc(sizeof(Pair));
    pair->first = first;
    pair->second = second;
    return pair;
}