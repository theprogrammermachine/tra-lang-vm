
#include <stdlib.h>
#include "array.h"

void initArray(Array *a, size_t initialSize) {
    a->array = (void**)malloc(initialSize * sizeof(void*));
    a->used = 0;
    a->size = initialSize;
}

void insertArray(Array *a, void* element) {
    // a->used is the number of used entries, because a->array[a->used++] updates a->used only *after* the array has been accessed.
    // Therefore a->used can go up to a->size
    if (a->used == a->size) {
        a->size += 100;
        a->array = (void**)realloc(a->array, a->size * sizeof(void*));
    }
    a->array[a->used++] = element;
}

void freeArray(Array *a) {
    free(a->array);
    a->array = NULL;
    a->used = a->size = 0;
}