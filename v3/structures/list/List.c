
#include "List.h"

void initList(struct List* list) {
    list->size = 0;
    list->listPointer = NULL;
    list->iteratorPointer = NULL;
    list->append = listAdd;
    list->iteratorForward = iteratorForward;
    list->iteratorHasNext = iteratorHasNext;
    list->iteratorBackward = iteratorBackward;
    list->iteratorHasBefore = iteratorHasBefore;
}

void listAdd(struct List* list, void* item) {
    struct ListDataItem* pItem = malloc(sizeof(struct ListDataItem));
    pItem->data = item;
    if (list->listPointer != NULL) {
        pItem->prev = list->listPointer;
        list->listPointer->next = pItem;
    }
    if (list->iteratorPointer == NULL) {
        list->iteratorPointer = pItem;
        list->iteratorPointer->next = NULL;
        list->iteratorPointer->prev = NULL;
    }
    list->listPointer = pItem;
    list->size++;
}

void* iteratorForward(struct List* list) {
    void* data = list->iteratorPointer->data;
    list->iteratorPointer = list->iteratorPointer->next;
    return data;
}

void* iteratorBackward(struct List* list) {
    void* data = list->iteratorPointer->data;
    list->iteratorPointer = list->iteratorPointer->prev;
    return data;
}

bool iteratorHasNext(struct List* list) {
    return (list->iteratorPointer != NULL);
}

bool iteratorHasBefore(struct List* list) {
    return (list->iteratorPointer != NULL);
}